package com.purcharoo.purcharoo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;


public class SearchActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //SearchView元件
    private List<ItemSearch> searchList;//搜尋資料
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SearchAdapter searchAdapter;

    //firestore
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;
    private ArrayList<String> schoolNameArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        searchList = new ArrayList<>();
        schoolNameArray = new ArrayList<>();

        //載入學校名
        setupSchoolName();

        //SearchView設置
        setupStore();

        //頂層toolbar設置
        Toolbar topToolbar = (Toolbar) findViewById(R.id.search_view_toolbar);
        setSupportActionBar(topToolbar);

        setupNavigationView(topToolbar); //側選單

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.search_view_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_tool_bar, menu);

        MenuItem searchItem = menu.findItem(R.id.search_bar);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setIconified(false);//搜尋欄默認已開啟
        searchView.setQueryHint("搜尋店家...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_acc_manage) {

            startActivity(new Intent(this , AccSettingActivity.class));
        } else if (id == R.id.nav_wallet) {


        } else if (id == R.id.nav_record) {

        } else if (id == R.id.nav_rate) {

        } else if (id == R.id.nav_loc_manage) {

            //進入SetPositionActivity
            startActivity(new Intent(this , SetPositionActivity.class));

        } else if (id == R.id.nav_story) {

        }else if(id == R.id.nav_sign_out){
            //isLogin還原為false
            Map<String,Object> changedata = new HashMap<>();
            changedata.put("isLogin" , false);
            firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                    .set(changedata , SetOptions.mergeFields("isLogin"));
            //登出
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this , LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.search_view_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setupSchoolName(){

    }

    //輸入搜尋資料
    private void setupStore() {

        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    schoolNameArray.add(documentSnapshot.getId());
                }
                for(int i = 0 ; i < schoolNameArray.size() ; i ++){
                    firestore.collection("school").document(schoolNameArray.get(i)).collection("store")
                            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                                        searchList.add(new ItemSearch(documentSnapshot.getString("storePic") , washStoreName(documentSnapshot.getString("storeName"))));
                                    }
                                    setupRecyclerView();
                                }
                            });
                }
            }
        });
    }

    private void setupRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        searchAdapter = new SearchAdapter(searchList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(searchAdapter);

        searchAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
               //itemclick method
            }
        });
    }

    public void setupNavigationView(Toolbar toolbar){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.search_view_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.search_view_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //firestore
        if(navigationView.getHeaderCount() > 0){
            View header = navigationView.getHeaderView(0);//取得nav的header及其物件

            final ImageView selfie = (ImageView)header.findViewById(R.id.nav_selfie);
            final TextView name = (TextView)header.findViewById(R.id.nav_name);
            final TextView account = (TextView)header.findViewById(R.id.nav_account);

            selfie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), CheckSelfieActivity.class));
                }
            });

            auth = FirebaseAuth.getInstance();
            firestore = FirebaseFirestore.getInstance();
            //處理帳號資料
            firestore.collection("memberData").document( auth.getCurrentUser().getUid()).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                name.setText(documentSnapshot.getString("name"));
                                account.setText(auth.getCurrentUser().getEmail());
                                Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                        //圖片使用最低分辨率,降低使用空間大小
                                        .fit()
                                        .centerCrop()
                                        .into(selfie);//取得大頭貼
                            }
                        }
                    });
        }
    }

    public String washStoreName(String dirtyStore){
        return dirtyStore.replace("_" , "-");
    }
}

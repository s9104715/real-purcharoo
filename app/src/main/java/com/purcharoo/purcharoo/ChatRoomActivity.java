package com.purcharoo.purcharoo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import at.markushi.ui.CircleButton;

public class ChatRoomActivity extends AppCompatActivity implements View.OnClickListener {

    private List<ItemChat> chats;
    private RecyclerView chatBar;
    private ChatAdapter chatAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private EditText inputET;
    private CircleButton inputBnt;

    //firebase
    private FirebaseFirestore firestore;
    private FirebaseAuth auth;
    private int lines;

    //監聽對方訊息
    private Timer msgTimer;
    private TimerTask msgTimerTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_chat_room);

        chatBar = findViewById(R.id.chat_bar);
        inputET = findViewById(R.id.input_ET);
        inputBnt = findViewById(R.id.send_text_bnt);
        chats = new ArrayList<>();

        inputBnt.setOnClickListener(this);

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        setupTopToolBar();
        setupRecyclerView();
        loadMsg();
        buildMsgSonar();
        activateMsgSonar();
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.chat_room_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setupRecyclerView() {

        chatBar.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        chatAdapter = new ChatAdapter(chats);
        chatBar.setLayoutManager(layoutManager);
        chatBar.setAdapter(chatAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.send_text_bnt:
                sendMsg();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        disableMsgSonar();
        super.onBackPressed();
    }

    //送出訊息
    public void sendMsg(){

        java.sql.Timestamp timestamp = getCurrentTime();//取得目前時間

        if(inputET.getText() == null || inputET.getText().equals("")){//沒打字
            //do nothing
        }else {
            final ItemChat itemChat = new ItemChat(auth.getCurrentUser().getUid() , inputET.getText().toString() , getTime(timestamp));
            chatAdapter.addMsg(itemChat);
            Toast.makeText(getApplicationContext() , timestamp.toString() , Toast.LENGTH_SHORT).show();//test

            //上傳至資料庫
            firestore.collection("task").document(getTaskID()).collection("chatRoom")
                    .document("room").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if(documentSnapshot.exists()){
                        //取得目前的行數(談話數)
                        int lineNum = documentSnapshot.get("lineNum" , Integer.TYPE);
                        //上傳
                        firestore.collection("task").document(getTaskID()).collection("chatRoom")
                                .document("room").collection("conversation").document(String.valueOf(lineNum))
                                .set(itemChat).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                //clear ET
                                inputET.setText("");
                            }
                        });
                        //修改lineNum
                        Map<String , Object> updateLineNum = new HashMap<>();
                        updateLineNum.put("lineNum" , lineNum + 1);
                        firestore.collection("task").document(getTaskID()).collection("chatRoom")
                                .document("room").set(updateLineNum , SetOptions.mergeFields("lineNum"));
                    }
                }
            });
        }
    }

    //取得目前時間
    public java.sql.Timestamp getCurrentTime(){

        java.sql.Timestamp timestamp = new java.sql.Timestamp(System.currentTimeMillis());
        return  timestamp;
    }

    //從timeStamp取得時間
    public String getTime(java.sql.Timestamp timestamp){
        String result = splitString(splitString(timestamp.toString() , " ")[0] , "-")[1]+"-"+
                        splitString(splitString(timestamp.toString() , " ")[0] , "-")[2]+" "+
                        splitString(splitString(timestamp.toString() , " ")[1] , ":")[0]+":"+
                        splitString(splitString(timestamp.toString() , " ")[1] , ":")[1];
        return result;
    }

    //從資料庫取得時間
    public String getTime(Date date){
        String result = splitString(splitString(date.toString() , " ")[3] , ":")[0]+":"+splitString(splitString(date.toString() , " ")[3] , ":")[1];
        return result;
    }

    //分割字串
    public String []splitString(String s , String regex){
        String[] split = s.split(regex);
        return split;
    }

    public String getTaskID(){

        String taskID = null;
        ArrayList<String> idList = new ArrayList<>();
        idList.add(AlertDialogMatchTaskActivity.taskID);
        idList.add(TaskInfoActivity.taskID);
        idList.add(OrderFragmentMyCurrentOrder.taskID);

        //還差一個接單中的任務明細

        //篩選是哪個有任務ID
        for(int i = 0 ; i < idList.size() ; i ++){
            if(idList.get(i) != null){
                taskID = idList.get(i);
            }
        }
        return taskID;
    }

    //讀取歷史訊息
    public void loadMsg(){

        firestore.collection("task").document(getTaskID()).collection("chatRoom")
                .document("room").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    //load conversation
                    for(int i = 0 ; i < documentSnapshot.get("lineNum" , Integer.TYPE) ; i++){
                        firestore.collection("task").document(getTaskID()).collection("chatRoom")
                                .document("room").collection("conversation").document(String.valueOf(i))
                                .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot lineDocumentSnapshot) {
                                if(lineDocumentSnapshot.exists()){
                                    //載入訊息
                                    chatAdapter.addMsg(new ItemChat(lineDocumentSnapshot.getString("userID") ,
                                                                    lineDocumentSnapshot.getString("text") ,
                                                                    getTime(lineDocumentSnapshot.get("time" , Timestamp.class).toDate())));
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    //監聽對方傳來的訊息
    public void buildMsgSonar(){

        msgTimer = new Timer();
        msgTimerTask = new TimerTask() {
            @Override
            public void run() {
                firestore.collection("task").document(getTaskID()).collection("chatRoom")
                        .document("room").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            //取得行數
                            //以行數監聽有沒有新訊息
                            firestore.collection("task").document(getTaskID()).collection("chatRoom")
                                    .document("room").collection("conversation")
                                    .document(String.valueOf(documentSnapshot.get("lineNum" , Integer.TYPE))).get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot newLineDocumentSnapshot) {
                                            if(newLineDocumentSnapshot.exists()){
                                                chatAdapter.addMsg(new ItemChat(newLineDocumentSnapshot.getString("userID") ,
                                                        newLineDocumentSnapshot.getString("text") ,
                                                        getTime(newLineDocumentSnapshot.get("time" , Timestamp.class).toDate())));
                                            }
                                        }
                                    });
                        }
                    }
                });
            }
        };
    }

    //執行監聽器
    public void activateMsgSonar(){
        msgTimer.schedule(msgTimerTask , 100 , 1000);//一秒執行一次
    }

    //取消監聽器
    public void disableMsgSonar(){
        msgTimer.cancel();
        msgTimer = null;
        msgTimerTask.cancel();
        msgTimerTask = null;
    }
}

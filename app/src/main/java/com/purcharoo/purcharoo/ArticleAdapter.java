package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ExampleViewHolder> implements Filterable {

    private List<ItemArticle> list;//過濾後的清單
    private List<ItemArticle> listFull;//完整清單
    private FirebaseFirestore firestore;

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入article_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        CircleImageView selfie;//大頭貼
        TextView articleTitle;//文章名
        TextView accout;//帳號
        TextView date;//日期

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener) {
            super(itemView);
            selfie = itemView.findViewById(R.id.selfie);
            articleTitle = itemView.findViewById(R.id.article_title);
            accout = itemView.findViewById(R.id.article_account);
            date = itemView.findViewById(R.id.date);
            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ArticleAdapter(List<ItemArticle> list) {
        this.list = list;
        listFull = new ArrayList<>(list);
    }

    //Adapter載入article_item.xml方法
    @NonNull
    @Override
    public ArticleAdapter.ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article,
                parent, false);
        ArticleAdapter.ExampleViewHolder viewHolder= new ArticleAdapter.ExampleViewHolder(v, clickListener);
        return viewHolder;
    }

    //顯示、更新item方法
    @Override
    public void onBindViewHolder(@NonNull final ArticleAdapter.ExampleViewHolder holder, int position) {
        ItemArticle currentItem = list.get(position);

        firestore = FirebaseFirestore.getInstance();
        //處裡圖片
        firestore.collection("memberData").document(currentItem.getUserUID()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                .fit()
                                .centerCrop()
                                .into(holder.selfie);
                    }
                });
        holder.articleTitle.setText(currentItem.getArticleName());
        holder.accout.setText(currentItem.getAccout());
        holder.date.setText(formatDate(currentItem.getDate()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    //過濾
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ItemArticle> filteredList = new ArrayList<>();

            //資料搜尋
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ItemArticle item : listFull) {
                    if (item.getArticleName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;//符合條件的資料

            return results;
        }

        //更新清單
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public String formatDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}

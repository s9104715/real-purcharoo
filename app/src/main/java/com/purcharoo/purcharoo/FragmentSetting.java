package com.purcharoo.purcharoo;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class FragmentSetting extends Fragment implements View.OnClickListener {

    //header元件
    private static ImageView selfie;
    private static TextView nameTV;
    //bnt
    private LinearLayout accInfoBnt;
    private LinearLayout walletBnt;
    private LinearLayout recordBnt;
    private LinearLayout ratingBnt;
    private LinearLayout loactionSetBnt;
    private LinearLayout storyBnt;

    //firestore
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_setting, container, false);

        selfie = view.findViewById(R.id.selfie);
        nameTV = view.findViewById(R.id.setting_name);

        accInfoBnt = view.findViewById(R.id.acc_info_bnt);
        walletBnt = view.findViewById(R.id.wallet_bnt);
        recordBnt = view.findViewById(R.id.record_bnt);
        ratingBnt = view.findViewById(R.id.rating_bnt);
        loactionSetBnt = view.findViewById(R.id.loaction_setting_bnt);
        storyBnt = view.findViewById(R.id.story_bnt);
        accInfoBnt.setOnClickListener(this);
        walletBnt.setOnClickListener(this);
        recordBnt.setOnClickListener(this);
        ratingBnt.setOnClickListener(this);
        loactionSetBnt.setOnClickListener(this);
        storyBnt.setOnClickListener(this);
        selfie.setOnClickListener(this);

        setupMemberData();//處理帳號資料

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.acc_info_bnt:
                startActivity(new Intent(getContext() , AccSettingActivity.class));
                break;
            case R.id.wallet_bnt:

                break;
            case R.id.record_bnt:

                break;
            case R.id.rating_bnt:

                break;
            case R.id.loaction_setting_bnt:
                startActivity(new Intent(getContext() , SetPositionActivity.class));
                break;
            case R.id.story_bnt:

                break;
            case R.id.selfie:
                startActivity(new Intent(getContext() , CheckSelfieActivity.class));
                break;
        }
    }

    //按鈕事件
    public void setBntClicked(LinearLayout bnt , final Class c){

        bnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext() , c));
            }
        });
    }

    public void setupMemberData(){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        //處理帳號資料
        firestore.collection("memberData").document( auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        nameTV.setText(documentSnapshot.getString("name"));
                        Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                //圖片使用最低分辨率,降低使用空間大小
                                .fit()
                                .centerCrop()
                                .into(selfie);
                    }
                });
    }

    public static void update(final String s){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        //處理帳號資料
        firestore.collection("memberData").document( auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        nameTV.setText(documentSnapshot.getString("name"));
                        Picasso.get().load(s)
                                //圖片使用最低分辨率,降低使用空間大小
                                .fit()
                                .centerCrop()
                                .into(selfie);
                    }
                });
    }
}

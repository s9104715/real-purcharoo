package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.markushi.ui.CircleButton;

public class FragmentShare extends Fragment {

    private TabLayout tabs;
    private ViewPager viewPager;
    private CircleButton editBnt;

    private ShareFragmentShareArticle schoolArticle;
    private ShareFragmentStoredArticle storedArticle;
    private ShareFragmentMyArticle myArticle;
    //EditArticleActivity usage
    public static boolean picUploaded = false;// false = articleTemp ; true = articleTempWithPic
    //firebase articleId
    public static String aritcleId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_share, container, false);


        tabs = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.share_view_pager);
        schoolArticle = new ShareFragmentShareArticle();
        storedArticle = new ShareFragmentStoredArticle();
        myArticle = new ShareFragmentMyArticle();

        //預設fragment
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.share_view_pager, new ShareFragmentShareArticle()).commit();

        setupTabsLayout();//tabsLayout設置

        //文章編輯按鈕
        editBnt = view.findViewById(R.id.edit_bnt);
        editBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              startActivity(new Intent(getContext() , EditArticleActivity.class));
            }
        });

        return view;
    }

    //tabLayout設置
    public void setupTabsLayout(){
        TabsAdapter adapter = new TabsAdapter(getFragmentManager());
        adapter.addFragment(schoolArticle , "所有文章");
        adapter.addFragment(storedArticle , "收藏");
        adapter.addFragment(myArticle , "我的文章");
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }
}

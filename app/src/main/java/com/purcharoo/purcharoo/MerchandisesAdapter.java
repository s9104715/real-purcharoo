package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import at.markushi.ui.CircleButton;

public class MerchandisesAdapter extends RecyclerView.Adapter<MerchandisesAdapter.ExampleViewHolder>{

    private List<ItemMerchandises> list;//清單
    private boolean onLongClickPermit;//是否允許長案事件

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入merchandises_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        private TextView merchandiseName;//商品名
        private TextView unitPrice;//單價
        private TextView num;//數量
        private TextView totalPrice;//總價
        private CircleButton deleteBnt;

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener , boolean onLongClickPermit) {
            super(itemView);

            merchandiseName = itemView.findViewById(R.id.merchandise_name);
            unitPrice = itemView.findViewById(R.id.unit_price);
            num = itemView.findViewById(R.id.num);
            totalPrice = itemView.findViewById(R.id.total_price);
            deleteBnt = itemView.findViewById(R.id.delete_bnt);

            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                    deleteBnt.setVisibility(View.INVISIBLE);
                }
            });

            //長按方法
            if(onLongClickPermit){
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        deleteBnt.setVisibility(View.VISIBLE);
                        return true;
                    }
                });
            }
        }
    }

    public MerchandisesAdapter(List<ItemMerchandises> list , boolean onLongClickPermit) {
        this.list = list;
        this.onLongClickPermit = onLongClickPermit;
    }

    //Adapter載入merchandises_item.xml方法
    @NonNull
    @Override
    public MerchandisesAdapter.ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_merchandises,
                parent, false);
        MerchandisesAdapter.ExampleViewHolder viewHolder= new MerchandisesAdapter.ExampleViewHolder(v, clickListener , onLongClickPermit);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ExampleViewHolder holder, final int position) {

        ItemMerchandises currentItem = list.get(position);

        holder.merchandiseName.setText(currentItem.getMerchandiseName());
        holder.unitPrice.setText("$"+String.valueOf(currentItem.getUnitPrice()));
        holder.num.setText(String.valueOf(currentItem.getNumber()));
        holder.totalPrice.setText("$"+String.valueOf(currentItem.getTotalPrice()));

        //刪除事件
        holder.deleteBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteOrder(position);
                holder.deleteBnt.setVisibility(View.INVISIBLE);

                //處理總和
                OrderAddListActivity.setupBotTotal(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //新增方法
    public void addOrder(ItemMerchandises merchandises){
        list.add(merchandises);
        notifyDataSetChanged();
    }

    //刪除特定item
    public void deleteOrder(int position){
        list.remove(position);
        notifyDataSetChanged();
    }

    public List<ItemMerchandises> getList() {
        return list;
    }
}

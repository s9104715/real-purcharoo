package com.purcharoo.purcharoo;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class OrderActivity extends AppCompatActivity {

    private TabLayout tabs;
    private ViewPager viewPager;
    private OrderFragmentOrders fragmentOrders;
    private OrderFragmentMyCurrentOrder fragmentMyCurrentOrder;
    //返回對話框
    private AlertDialog backAlertDialog;
    //此活動
    public static OrderActivity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_order);

        tabs = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.view_pager);
        fragmentOrders = new OrderFragmentOrders();
        fragmentMyCurrentOrder = new OrderFragmentMyCurrentOrder();
        instance = this;

        //預設Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.view_pager, new OrderFragmentOrders()).commit();

        setupTopToolBar();
        setupTabsLayout();//tabsLayout設置
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.order_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogPopUp();//返回alertDialog出現
            }
        });
    }

    @Override
    public void onBackPressed() {
        OrderFragmentMyCurrentOrder.taskID = null;//clear
        alertDialogPopUp();//返回alertDialog出現
    }

    //tabLayout設置
    public void setupTabsLayout(){
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(fragmentOrders , "創建任務");
        adapter.addFragment(fragmentMyCurrentOrder , "目前任務");
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }

    //返回對話框
    public void alertDialogPopUp(){

        final AlertDialog.Builder ADBuider = new AlertDialog.Builder(OrderActivity.this);
        ADBuider.setTitle("訂單任務");
        ADBuider.setMessage("請問您是否要返回上一頁？");

        DialogInterface.OnClickListener ADClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case DialogInterface.BUTTON_POSITIVE:
                        backAlertDialog.dismiss();
                        OrderActivity.super.onBackPressed();
                        OrderFragmentOrders.hasSelectTime = false;//是否已選擇時間 ----> 回歸初始化
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        backAlertDialog.dismiss();
                        break;
                }
            }
        };

        ADBuider.setPositiveButton("是" , ADClickListener);
        ADBuider.setNegativeButton("否" , ADClickListener);

        backAlertDialog = ADBuider.create();
        backAlertDialog.show();
    }

    @Override
    public void finish() {
        super.finish();
        instance = null;
    }
}


package com.purcharoo.purcharoo;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class FragmentMainPage extends Fragment implements OnMapReadyCallback {

    public static ScrollView scrollView;
    //主頁店家滑塊元件
    private ViewPager storeSliderViewPager;
    private ViewPagerAdapter storeSliderAdapter;
    //下方四個RadioBnt
    private RadioGroup radioGroup;
    private RadioButton bnt1;
    private RadioButton bnt2;
    private RadioButton bnt3;
    private RadioButton bnt4;

    //Firebase
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    private static StorageReference storageRef;
    private ArrayList<String> promotionStoreName;
    private ArrayList<String> promotionPicUri;

    //map
    private GoogleMap mGoogleMap;
    private SupportMapFragment mapFrag;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    public static FusedLocationProviderClient mFusedLocationClient;
    private LatLng address1;
    private List<String> schoolArray;
    public static boolean mapIsTouched = false;//偵測地圖有無被碰觸


    public FragmentMainPage() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_page, container, false);


        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        promotionStoreName = new ArrayList<>();
        promotionPicUri = new ArrayList<>();
        schoolArray = new ArrayList<>();

        scrollView = view.findViewById(R.id.sv);
        //主頁店家滑塊設定
        storeSliderViewPager = (ViewPager)view.findViewById(R.id.main_viewPager);
        radioGroup = (RadioGroup)view.findViewById(R.id.main_radiogroup);
        bnt1 = (RadioButton)view.findViewById(R.id.radioButton1);
        bnt2 = (RadioButton)view.findViewById(R.id.radioButton2);
        bnt3 = (RadioButton)view.findViewById(R.id.radioButton3);
        bnt4 = (RadioButton)view.findViewById(R.id.radioButton4);
        setupPromotionPic();
        setupMarket();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        return view;
    }


    public void setupPromotionPic(){

        //取得推播店名
        firestore.collection("promotionStore").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    promotionStoreName.add(documentSnapshot.getId());
                }
                //取得Uri
                for(int i = 0 ; i < promotionStoreName.size() ; i ++){
                    storageRef.child("storePic/"+ promotionStoreName.get(i)+".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            promotionPicUri.add(uri.toString());
                            //滿4張即載入ViewPager
                            if(promotionPicUri.size() == 4){
                                setupStoreSliderViewPager();
                            }
                        }
                    });
                }
            }
        });
    }

    public void setupStoreSliderViewPager(){

        storeSliderAdapter = new ViewPagerAdapter(promotionPicUri , getContext() , false);
        storeSliderViewPager.setAdapter( storeSliderAdapter);

//        //viewPager自動滑動
//        final Handler handler = new Handler();
//        final Runnable update = new Runnable() {
//            int currentPage = storeSliderViewPager.getCurrentItem();
//            public void run() {
//                if (currentPage > images.length -1) {
//                    currentPage = 0;
//                }
//                storeSliderViewPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(update);
//            }
//        }, 100, 7000);//移動間隔

        //disable 4個RadioButton
        bnt1.setClickable(false);
        bnt2.setClickable(false);
        bnt3.setClickable(false);
        bnt4.setClickable(false);

        //ViewPager監聽事件
        storeSliderViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            //GroupButton移動功能
            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        radioGroup.check(R.id.radioButton1);
                        break;
                    case 1:
                        radioGroup.check(R.id.radioButton2);
                        break;
                    case 2:
                        radioGroup.check(R.id.radioButton3);
                        break;
                    case 3:
                        radioGroup.check(R.id.radioButton4);
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }
    @Override
    public void onPause() {
        super.onPause();

        //不用一職 更新
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //位置允許
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //檢查
                checkLocationPermission();
            }
        }
        else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //最新的位置第一個
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //設定現在位置的標籤
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("位置").snippet("您目前的位置");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);


                //move map camera
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            } else {

            }
        }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //允許
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    //權限不被允許
                    Toast.makeText(getContext(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }

    }

    //設定標籤
    public void setupMarket(){

        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    schoolArray.add(documentSnapshot.getId());
                }
                for(int i = 0 ; i < schoolArray.size() ; i ++){
                    firestore.collection("school")
                            .document(schoolArray.get(i))
                            .collection("store").addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        //存取資料庫偏好位置
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                                if (documentSnapshot.exists()) {
                                    GeoPoint geo = documentSnapshot.getGeoPoint("position");
                                    String title = documentSnapshot.getString("storeName");
                                    String phone = documentSnapshot.getString("phone");
                                    String address = documentSnapshot.getString("address");
                                    double lat = geo.getLatitude();
                                    double lng = geo.getLongitude();
                                    address1 = new LatLng(lat, lng);
                                    if(getActivity() != null){
                                        mGoogleMap.setInfoWindowAdapter(new InfoWindowAdapter(getActivity()));
                                        mGoogleMap.addMarker(new MarkerOptions().position(address1).title(title)
                                                .snippet(address+"\n"+phone));
                                    }
                                }
                            }
                        };
                    });
                }
            }
        });
    }

}
package com.purcharoo.purcharoo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class OrderMapActivity extends AppCompatActivity
        implements OnMapReadyCallback {


    private SupportMapFragment mapFrag;
    private LatLng set;
    private GoogleMap mGoogleMap;
    private LatLng latLng;
    public static double latitude;
    public static double longitude;
    private String address;
    private List<MarkerOptions> markers;
    public static String selectedAddress; //選擇地址

    //firebase
    private FirebaseFirestore firestore;
    private FirebaseAuth auth;

    //此活動
    public static OrderMapActivity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_order_map);

        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        markers  = new ArrayList<>();
        instance = this;

        setupTopToolBar();
        setPosition();
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.order_map_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        setupMarkerOnClick();
    }

    //載入已設定的送貨地點
    public void setPosition() {
        firestore.collection("memberData")
                .document(auth.getCurrentUser().getUid())
                .collection("positionPreference").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            //存取資料庫偏好位置
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    if (documentSnapshot.exists()) {
                        if(documentSnapshot.getDouble("latitude") != null && (documentSnapshot.getDouble("longitude") != null)){
                            double lat = documentSnapshot.getDouble("latitude");
                            double lng = documentSnapshot.getDouble("longitude");
                            latLng = new LatLng(lat, lng);
                            String tv = documentSnapshot.getString("address");
                            address = tv;
                            //創造marker
                            MarkerOptions marker = new MarkerOptions().position(latLng).title("送餐到").snippet(address);
                            mGoogleMap.addMarker(marker);
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                            //塞入marker清單資料
                            markers.add(marker);
                        }
                    }
                };
            }
        });
    }

    //選擇送貨地點
    public void setupMarkerOnClick(){

        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {
                new AlertDialog.Builder(OrderMapActivity.this)
                        .setTitle("選擇位置")
                        .setMessage("請問您是否要送餐到此位置？")
                        .setPositiveButton("確認", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                //設置位置資訊
                                selectedAddress = marker.getSnippet();//存取選擇地址
                                //經緯度
                                latitude = marker.getPosition().latitude;
                                longitude = marker.getPosition().longitude;

                                startActivity(new Intent(OrderMapActivity.this , OrderFinalCheckoutActivity.class));
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                return;
                            }
                        }).show();
            }
        });
    }

    @Override
    public void finish() {
        super.finish();
        instance = null;
    }
}
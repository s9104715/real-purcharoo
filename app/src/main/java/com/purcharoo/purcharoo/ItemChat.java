package com.purcharoo.purcharoo;

public class ItemChat {

    private String userUID;
    private String text;
    private String time;

    public ItemChat(){
    }

    public ItemChat(String userUID, String text, String time) {
        this.userUID = userUID;
        this.text = text;
        this.time = time;
    }

    public String getUserUID() {
        return userUID;
    }

    public void setUserUID(String userUID) {
        this.userUID = userUID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

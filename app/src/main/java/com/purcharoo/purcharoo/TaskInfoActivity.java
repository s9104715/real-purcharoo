package com.purcharoo.purcharoo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.model.value.ServerTimestampValue;
import com.google.firestore.v1.DocumentTransform;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import de.hdodenhof.circleimageview.CircleImageView;

public class TaskInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private ConstraintLayout userBar;//上層使用者區塊
    private CircleImageView selfie;
    private TextView name;
    private TextView account;
    private TextView school;
    //清單
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private OrderAdapter orderAdapter;
    public static ArrayList<ItemOrder> list;

    private TextView payment;
    private TextView time;
    private TextView remark;
    private TextView location;
    private Button checkMapBnt;
    private Button acceptTaskBnt;
    private ScrollView sv;
    public static String taskID;//餵給聊天室

    //接受任務對話框
    private AlertDialog acceptAlertDialog;
    private Boolean isAssigned = false;
    //processDialog
    private ProgressDialog progressDialog;
    //AssignedSonar監聽任務
    private Timer assignedSonarTimer;
    private TimerTask assignedSonarTimerTask;
    //match監聽任務
    private Timer matchTimer;
    private TimerTask matchTimerTask;
    //訂單者過久無回應的timer 80秒後視為媒合失敗
    private Timer userNotRespondTimer;
    private TimerTask userNotRespondTimerTask;
    private int runTime = 0;//該任務執行次數

    //firestore
    private  FirebaseAuth auth;
    private  FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_task_info);

        userBar = findViewById(R.id.user_bar);
        selfie = findViewById(R.id.selfie);
        name = findViewById(R.id.name);
        account = findViewById(R.id.account);
        school = findViewById(R.id.school);
        recyclerView = findViewById(R.id.order_list);
        payment = findViewById(R.id.payment);
        time = findViewById(R.id.time);
        remark = findViewById(R.id.remark);
        location = findViewById(R.id.location);
        checkMapBnt = findViewById(R.id.check_map_bnt);
        acceptTaskBnt = findViewById(R.id.accept_bnt);
        sv = findViewById(R.id.sv);

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        checkMapBnt.setOnClickListener(this);
        acceptTaskBnt.setOnClickListener(this);

        setTaskID();//taskID餵給聊天室
        setupTopToolBar();
        setupRecyclerView();
        loadTaskInfo();//載入任務資訊
        buildAssignedSonar();//建置是否被接單的監聽器
        activateAssignedSonar();//啟動是否被接單的監聽器
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.task_info_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(TaskInfoActivity.this);
        orderAdapter = new OrderAdapter( TaskFragmentTasks.itemTask.getOrders() , false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);

        //查看清單
        orderAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(!orderAdapter.getViewHolder().isItemLongClick()){//刪除鍵已出現
                    OrderCheckListActivity.setupList(TaskFragmentTasks.itemTask.getOrders().get(position).getMerchandiseList());
                    startActivity(new Intent(TaskInfoActivity.this , OrderCheckListActivity.class));
                }
            }
        });
    }

    //載入任務資訊
    public void loadTaskInfo(){

        //載入大頭貼
        firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            Picasso.get()
                                    .load(documentSnapshot.getString("selfiePath"))
                                    //圖片使用最低分辨率,降低使用空間大小
                                    .fit()
                                    .centerCrop()
                                    .into(selfie);
                            name.setText(documentSnapshot.getString("name"));
                            account.setText(documentSnapshot.getString("account"));
                            school.setText(documentSnapshot.getString("school"));
                        }
                    }
                });
        payment.setText("還沒好");
        time.setText(TaskFragmentTasks.itemTask.getTime());
        remark.setText(TaskFragmentTasks.itemTask.getRemark());
        location.setText(TaskFragmentTasks.itemTask.getAddress());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.check_map_bnt:
                startActivity(new Intent(getApplicationContext() , TaskCheckMapActivity.class));
                break;
            case R.id.accept_bnt:
                alertDialogPopUp();
                break;
        }
    }

    //返回對話框
    public void alertDialogPopUp(){

        final AlertDialog.Builder ADBuider = new AlertDialog.Builder(TaskInfoActivity.this);
        ADBuider.setTitle("接單任務");
        ADBuider.setMessage("請問您是否要接下此任務？");

        DialogInterface.OnClickListener ADClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case DialogInterface.BUTTON_POSITIVE:
                        acceptAlertDialog.dismiss();
                        //accept
                        contact();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        acceptAlertDialog.dismiss();
                        break;
                }
            }
        };

        ADBuider.setPositiveButton("是" , ADClickListener);
        ADBuider.setNegativeButton("否" , ADClickListener);

        acceptAlertDialog = ADBuider.create();
        acceptAlertDialog.show();
    }

    //偵測任務是否被接走
    public void buildAssignedSonar(){

        assignedSonarTimer = new Timer();
        assignedSonarTimerTask = new TimerTask() {
            @Override
            public void run() {
                firestore.collection("task").document(TaskFragmentTasks.itemTask.getTaskID()).get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if(documentSnapshot.exists()){
                                    if(documentSnapshot.getBoolean("assigned") == true){
                                        isAssigned = true;
                                    }
                                }
                            }
                        });
            }
        };
    }

    public void activateAssignedSonar(){
        assignedSonarTimer.schedule(assignedSonarTimerTask , 0 , 3000);//每3秒針測一次任務有沒有被接走
    }

    //聯絡對方------> 1.被對方接受 2.被對方拒絕
    public void contact(){

        //取消AssignedSonar
        disableAssignedSonar();

        if(!isAssigned){

            //修改訂單者myCurrentTask的contact為true來聯絡對方
            Map<String , Object> contact = new HashMap<>();
            contact.put("contact" , true);
            firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).collection("myCurrentTask")
                    .document("task").set(contact , SetOptions.mergeFields("contact"));
            //修改訂單者myCurrentTask的contacter為自身UID
            Map<String , Object> contacter = new HashMap<>();
            contacter.put("contacter" , auth.getCurrentUser().getUid());
            firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).collection("myCurrentTask")
                    .document("task").set(contacter , SetOptions.mergeFields("contacter"));
            buildMatchTask();
            activateMatchTask();

        }else {

            Toast.makeText(getApplicationContext() , "此訂單已經被別人接走！" , Toast.LENGTH_SHORT).show();
            //刷新TaskFragmentTasks
            finish();
            if(TaskActivity.instance != null){
                TaskActivity.instance.finish();
            }
            startActivity(new Intent(getApplicationContext() , TaskActivity.class));
        }
    }

    //媒合
    public void buildMatchTask(){

        matchDialog();//process dialog開始跑

        //UserNotRespondTask開始偵測 80秒後視同媒合失敗
        buildUserNotRespondTask();
        activateUserNotRespondTask();

        matchTimer = new Timer();
        matchTimerTask = new TimerTask() {
            @Override
            public void run() {
                firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).collection("myCurrentTask")
                        .document("task").get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if(documentSnapshot.exists()){
                                    if(documentSnapshot.getBoolean("assigned")) {
                                        //接單成功
                                        setProgressTime(progressDialog, 1000 , false);
                                        //取消媒合監聽任務
                                        disableMatchTimer();
                                        disableUserNotRespondTimer();
                                    }else if(!documentSnapshot.getBoolean("contact")){
                                        //接單不成功
                                        setProgressTime(progressDialog , 1000 , true);
                                        //取消媒合監聽任務
                                        disableMatchTimer();
                                        disableUserNotRespondTimer();
                                    }
                                }
                            }
                        });
            }
        };
    }

    public void activateMatchTask(){
        //2秒後延遲執行(讓資料庫有時間更改資料)
        matchTimer.schedule(matchTimerTask , 2000 , 1000);
    }

    //媒合 process dialog
    private void matchDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("正在等待對方回應");
        progressDialog.setMessage("媒合中.....");
        progressDialog.show();
    }

    public void setProgressTime(final ProgressDialog progressDialog , int delayMillis , final boolean doNotThing){

        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                if(!doNotThing){
                    progressDialog.dismiss();

                    //新增taskTaking到memberData
                    Map<String , Object> taskTaking = new HashMap<>();
                    taskTaking.put("taskID" , TaskFragmentTasks.itemTask.getTaskID());
                    taskTaking.put("user" , TaskFragmentTasks.itemTask.getUserUID());
                    taskTaking.put("time" ,  getTimeStamp());
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("taskTaking")
                            .document(TaskFragmentTasks.itemTask.getTaskID()).set(taskTaking);

                    //跳轉到訂單者資訊及查看其所在位置
                    startActivity(new Intent(getApplicationContext() , ChatRoomActivity.class));
                    taskID = null;//clear
                    //taskInfo、TaskActivity結束
                    finish();
                    if(TaskActivity.instance != null){
                        TaskActivity.instance.finish();
                    }
                }else {
                    progressDialog.dismiss();
                    //刷新此頁面
                    finish();
                    startActivity(new Intent(getApplicationContext() , TaskInfoActivity.class));
                    Toast.makeText(getApplicationContext() , "對方已拒絕了你的請求！" , Toast.LENGTH_SHORT).show();
                }
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, delayMillis);//毫秒計算
    }

    //訂單者過久無回應監聽器
    public void buildUserNotRespondTask(){

        userNotRespondTimer = new Timer();
        userNotRespondTimerTask = new TimerTask() {
            @Override
            public void run() {
                runTime++;
                if(runTime == 70){

                    //等待時間過長(70秒)  是為媒合失敗
                    progressDialog.dismiss();
                    //修改對方資料庫contact、 contacter
                    Map<String,Object> contact = new HashMap<>();
                    contact.put("contact" , false);
                    firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).collection("myCurrentTask")
                            .document("task")
                            .set(contact , SetOptions.mergeFields("contact"));
                    //contacter變為空
                    Map<String,Object> contacter = new HashMap<>();
                    contacter.put("contacter" , "");
                    firestore.collection("memberData").document(TaskFragmentTasks.itemTask.getUserUID()).collection("myCurrentTask")
                            .document("task")
                            .set(contacter , SetOptions.mergeFields("contacter"));
                    runTime = 0;//歸零
                    disableMatchTimer();
                    disableUserNotRespondTimer();
                    //刷新此頁面
                    finish();
                    startActivity(new Intent(getApplicationContext() , TaskInfoActivity.class));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext() , "對方無回應！" , Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        };
    }

    public void activateUserNotRespondTask(){
        userNotRespondTimer.schedule(userNotRespondTimerTask , 100 , 1000);//執行一次需一秒
    }

    //取消偵測assigned監聽器
    public void disableAssignedSonar(){
        assignedSonarTimer.cancel();
        assignedSonarTimer = null;
        assignedSonarTimerTask.cancel();
        assignedSonarTimerTask = null;
    }

    //取消媒合監聽任務
    public void disableMatchTimer(){
        matchTimer.cancel();
        matchTimer = null;
        matchTimerTask.cancel();
        matchTimerTask = null;
    }

    //取消訂單過久無回應監聽器
    public void disableUserNotRespondTimer(){
        userNotRespondTimer.cancel();
        userNotRespondTimer = null;
        userNotRespondTimerTask.cancel();
        userNotRespondTimerTask = null;
    }

    //取得時間
    public java.sql.Timestamp getTimeStamp(){
        Date d = new Date();
        java.sql.Timestamp timestamp = new java.sql.Timestamp(d.getTime());
        return timestamp;
    }

    public void setTaskID(){

        taskID = TaskFragmentTasks.itemTask.getTaskID();
    }

//    //依序手機長寬自動調整 SV
//    public void setupSVHeight(){
//
//        //get screen height
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int screenHeight = displayMetrics.heightPixels;
//
//        //螢幕高度- userbar高度 = SV高度
//        Constraints.LayoutParams cLp = new Constraints.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT , ConstraintLayout.LayoutParams.WRAP_CONTENT);
//        cLp.height = screenHeight - userBar.getLayoutParams().height;
//        cLp.width = sv.getLayoutParams().width;
//
//        //SV設定高度
//        sv.setLayoutParams(cLp);
//
//    }
}

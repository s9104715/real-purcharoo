package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ExampleViewHolder> implements Filterable {
    private List<ItemSearch> list;//過濾後的清單
    private List<ItemSearch> listFull;//完整清單

    //點擊效果
    private OnItemClickListener clickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入search_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {
        CircleImageView storeImage;//商家照片
        TextView storeName;//店名

        private ExampleViewHolder(View itemView , final OnItemClickListener listener) {
            super(itemView);
            storeImage = itemView.findViewById(R.id.image_view);
            storeName = itemView.findViewById(R.id.store_name_text);
            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

        public SearchAdapter(List<ItemSearch> list) {
            this.list = list;
            listFull = new ArrayList<>(list);
    }

    //Adapter載入search_item.xml方法
    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store,
                parent, false);
        ExampleViewHolder viewHolder= new ExampleViewHolder(v, clickListener);
        return viewHolder;
    }

    //顯示、更新item方法
    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        ItemSearch currentItem = list.get(position);

        Picasso.get().load(currentItem.getStorePicUri())
                //圖片使用最低分辨率,降低使用空間大小
                .fit()
                .centerCrop()
                .into(holder.storeImage);//取得大頭貼
        holder.storeName.setText(currentItem.getStoreText());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    //過濾
    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ItemSearch> filteredList = new ArrayList<>();

            //資料搜尋
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ItemSearch item : listFull) {
                    if (item.getStoreText().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;//符合條件的資料

            return results;
        }

        //更新清單
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}

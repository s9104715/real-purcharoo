package com.purcharoo.purcharoo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

public class OrderAddListActivity extends AppCompatActivity {

    private Spinner schoolSpinner;
    private Spinner storeSpinner;

    private RecyclerView.LayoutManager layoutManager;
    //商品清單
    private RecyclerView merchandiseRecyclerview;
    private MerchandiseAdapter merchandiseAdapter;
    private ArrayList<ItemMerchandise> merchandiseList;
    //底部清單
    private RecyclerView botRecyclerView;
    public static MerchandisesAdapter merchandisesAdapter;
    private ArrayList<ItemMerchandises> merchandisesList;

    private CardView selectedCard;
    private TextView cardMerchandiseName;
    private TextView cardMerchandisePrice;
    private TextView numTV;
    private EditText numET;
    private TextView totalPriceTV;
    private TextView totalPrice;//單筆total
    private Button confirmBnt;//商品確認紐
    private CircleButton confirmCircleBnt;//訂單確認
    public static TextView totalTV;
    public static TextView total;//總total

    //Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;
    private ArrayList<String> storeList;
    private String documentID;
    private String storeName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_order_add_list);

        schoolSpinner = findViewById(R.id.school_spinner);
        storeSpinner = findViewById(R.id.store_spinner);
        merchandiseRecyclerview = findViewById(R.id.merchandise_list);
        botRecyclerView = findViewById(R.id.merchandises_list);
        selectedCard = findViewById(R.id.selected_card);
        cardMerchandiseName = findViewById(R.id.card_merchandise_name);
        cardMerchandisePrice = findViewById(R.id.card_merchandise_price);
        numTV = findViewById(R.id.num_TV);
        numET = findViewById(R.id.num_ET);
        totalPriceTV = findViewById(R.id.total_price_tv);
        totalPrice = findViewById(R.id.total_price);
        confirmBnt = findViewById(R.id.confirm_bnt);
        confirmCircleBnt = findViewById(R.id.confirm_circle_bnt);
        totalTV = findViewById(R.id.total_tv);
        total = findViewById(R.id.total);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        merchandisesList = new ArrayList<>();

        //設置元件
        selectedCard.setVisibility(View.INVISIBLE);
        numTV.setVisibility(View.INVISIBLE);
        numET.setEnabled(false);
        totalTV.setVisibility(View.INVISIBLE);
        total.setVisibility(View.INVISIBLE);

        //隱藏totalPrice的兩個元件
        totalPriceTV.setVisibility(View.INVISIBLE);
        totalPrice.setVisibility(View.INVISIBLE);

        setupTopToolBar();
        setupSchSpinner();//學校spinner設置
        setupSchSpinnerItemOnCllick();//學校spinner點擊方法
        setupStoreSpinnerItemOnClick();//商店spinner點擊方法
        setupBotRecyclerview();//設置底部清單
        setupConfirmCircleBnt();//設置確認訂單扭
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.order_add_list_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //學校spinner設置
    public void setupSchSpinner(){

        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                List<String> schList = new ArrayList<>();
                schList.add("");//空字串
                for(DocumentSnapshot snapshot : queryDocumentSnapshots){
                    schList.add(snapshot.getString("name"));
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext() , R.layout.spinner_item ,schList);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                schoolSpinner.setAdapter(spinnerAdapter);//載入學校清單
            }
        });
    }

    //商店spinner設置
    public void setupSchSpinnerItemOnCllick(){

        schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        //空字串不做任何動作
                        break;
                    case 1:
                        //輔大
                        setupStoreSpinner("輔仁大學");
                        schoolSpinner.setEnabled(false);//鎖定
                        break;
                    case 2:
                        //政大
                        setupStoreSpinner("政治大學");
                        schoolSpinner.setEnabled(false);//鎖定
                        break;
                    case 3:
                        //台大
                        setupStoreSpinner("台灣大學");
                        schoolSpinner.setEnabled(false);//鎖定
                        break;
                    case 4:
                        //文大
                        setupStoreSpinner("文化大學");
                        schoolSpinner.setEnabled(false);//鎖定
                        break;
                    case 5:
                        //淡大
                        setupStoreSpinner("淡江大學");
                        schoolSpinner.setEnabled(false);//鎖定
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setupStoreSpinner(final String school){

        storeList = new ArrayList<>();
        storeList.add("");//空字串
        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    //符合學校字串
                    if(documentSnapshot.getString("name").equals(school)){

                        documentID = documentSnapshot.getId();//存取學校ID給loadMerchandise用

                        firestore.collection("school").document(documentSnapshot.getId()).collection("store")
                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                        //載入店名
                                        for(DocumentSnapshot documentSnapshot1 : queryDocumentSnapshots){
                                            storeList.add(documentSnapshot1.getString("storeName"));
                                        }
                                        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext() , R.layout.spinner_item , storeList);
                                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        storeSpinner.setAdapter(spinnerAdapter);
                                    }
                                });
                    }
                }
            }
        });
    }

    public void setupStoreSpinnerItemOnClick(){

        storeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        //空字串不做任何動作
                        break;
                    case 1:
                        loadMerchandise(storeList.get(1));
                        storeSpinner.setEnabled(false);//鎖定
                        break;
                    case 2:
                        loadMerchandise(storeList.get(2));
                        storeSpinner.setEnabled(false);//鎖定
                        break;
                    case 3:
                        loadMerchandise(storeList.get(3));
                        storeSpinner.setEnabled(false);//鎖定
                        break;
                    case 4:
                        loadMerchandise(storeList.get(4));
                        storeSpinner.setEnabled(false);//鎖定
                        break;
                    case 5:
                        loadMerchandise(storeList.get(5));
                        storeSpinner.setEnabled(false);//鎖定
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void loadMerchandise(final String store){

        merchandiseList = new ArrayList<>();//商品清單

        firestore.collection("school").document(documentID).collection("store")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                        for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            if(documentSnapshot.getString("storeName").equals(store)){
                                storeName = store;//存取店名
                                firestore.collection("school").document(documentID).collection("store")
                                        .document(documentSnapshot.getId()).collection("merchandise").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                    @Override
                                    public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                        for(DocumentSnapshot documentSnapshot1 :queryDocumentSnapshots){
                                            if(documentSnapshot1.get("unitPrice" , Integer.TYPE) != null){// 防止指向 NullPointerException
                                                merchandiseList.add(new ItemMerchandise(documentSnapshot1.getString("name") , documentSnapshot1.get("unitPrice" , Integer.TYPE)));
                                            }
                                        }
                                        setupMerchandiseList(merchandiseList);//載入至recyclerview
                                    }
                                });
                            }
                        }
                    }
                });
    }

    //載入商品recyclerview
    public void setupMerchandiseList(final ArrayList<ItemMerchandise> merchandiseList){

        merchandiseRecyclerview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext() , LinearLayoutManager.HORIZONTAL , false);//水平排版
        merchandiseAdapter = new MerchandiseAdapter(merchandiseList);
        merchandiseRecyclerview.setLayoutManager(layoutManager);
        merchandiseRecyclerview.setAdapter(merchandiseAdapter);

        merchandiseAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position) {

                selectedCard.setVisibility(View.VISIBLE);
                cardMerchandiseName.setText(merchandiseList.get(position).getMerchandiseName());
                cardMerchandisePrice.setText("$ "+merchandiseList.get(position).getUnitPrice());
                numTV.setVisibility(View.VISIBLE);
                numET.setEnabled(true);
                //顯示totalPrice兩個元件
                totalPriceTV.setVisibility(View.VISIBLE);
                totalPrice.setVisibility(View.VISIBLE);

                numET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if(charSequence != null){
                            //計算總價
                            if(charSequence.toString().equals("")){
                                totalPrice.setText("$ 0");//numET沒數字時
                            }else {
                                totalPrice.setText("$ "+(merchandiseList.get(position).getUnitPrice()*Integer.valueOf(numET.getText().toString())));
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                confirmBnt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(numET.getText().toString().equals("") || numET.getText().toString().equals("0")){//沒數字或0
                            numET.setError("請輸入商品數量");
                            numET.requestFocus();
                        }else {
                            //新增merchandisesItem
                            merchandisesAdapter.addOrder(new ItemMerchandises(merchandiseList.get(position).getMerchandiseName(), merchandiseList.get(position).getUnitPrice() , Integer.valueOf(numET.getText().toString())));
                            //部分元件初始化
                            selectedCard.setVisibility(View.INVISIBLE);
                            numET.setText("");
                            numTV.setVisibility(View.INVISIBLE);
                            numET.setEnabled(false);
                            totalPriceTV.setVisibility(View.INVISIBLE);
                            totalPrice.setVisibility(View.INVISIBLE);
                            confirmBnt.setOnClickListener(null);
                            //計算總和
                            setupBotTotal(true);
                        }
                    }
                });
            }
        });
    }

    //設置底部清單
    public void setupBotRecyclerview(){

        botRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());//垂直排版
        merchandisesAdapter = new MerchandisesAdapter(merchandisesList , true);
        botRecyclerView.setLayoutManager(layoutManager);
        botRecyclerView.setAdapter(merchandisesAdapter);
    }

    //設置確認訂單扭
    public void setupConfirmCircleBnt(){

        confirmCircleBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(merchandisesList.size() > 0){//有清單
                    OrderFragmentOrders.addOrder(getItemOrder(merchandisesList));//新增訂單
                    OrderFragmentOrders.setupTotal(true);//處理OrderActivity的總和
                    finish();
                }else {//無清單
                    Toast.makeText(getApplicationContext() , "您尚未選擇商品" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //取得ItemOrder
    public ItemOrder getItemOrder(ArrayList<ItemMerchandises> merchandisesList){

        String detail;
        if(merchandisesList.size() == 1){//僅一項商品
            detail = "包含"+merchandisesList.get(0).getMerchandiseName()+"一項商品";
        }else {
            detail = "包含"+merchandisesList.get(0).getMerchandiseName()+"等"+String.valueOf(merchandisesList.size())+"項商品";
        }
        return new ItemOrder(storeName , detail , merchandisesList);
    }

    //設置總計
    public static void setupBotTotal(boolean show){

        int sum = 0;//總和
        if(show){//有merchandises時
            //總計顯示
            totalTV.setVisibility(View.VISIBLE);
            total.setVisibility(View.VISIBLE);
            //計算
            for(int i = 0 ; i < merchandisesAdapter.getList().size() ; i ++){
                sum += merchandisesAdapter.getList().get(i).getTotalPrice();
            }
            total.setText("$ "+String.valueOf(sum));
        }else {
            if(merchandisesAdapter.getList().size() == 0){
                //無清單時隱藏
                totalTV.setVisibility(View.INVISIBLE);
                total.setVisibility(View.INVISIBLE);
                total.setText("");
            }else {
                //計算
                for(int i = 0 ; i < merchandisesAdapter.getList().size() ; i ++){
                    sum += merchandisesAdapter.getList().get(i).getTotalPrice();
                }
                total.setText("$ "+String.valueOf(sum));
            }
        }

    }
}

package com.purcharoo.purcharoo;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import at.markushi.ui.CircleButton;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderFragmentMyCurrentOrder extends Fragment {

    private TextView noTaskTV;

    //recyclerview
    private RecyclerView merchandiseList;
    public static List<ItemOrder> orders;//訂單清單
    private RecyclerView.LayoutManager layoutManager;
    public static OrderAdapter orderAdapter;

    private TextView totalTV;
    private TextView total;
    private TextView addressTV;
    private TextView address;
    private TextView timeTV;
    private TextView time;
    private TextView remarkTV;
    private TextView remark;
    private TextView taskTakerTV;
    private CardView takerCard;
    private CircleImageView selfie;
    private TextView name;
    private TextView account;
    private TextView school;
    private CircleButton checkPositionBnt;
    private CircleButton chatBnt;
    private TextView noTakerTV;
    public static String taskID;//餵給聊天室

    //返回對話框
    private AlertDialog detectOverTimeTaskAlertDialog;

    //Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_order_my_current_order, container, false);

        noTaskTV = view.findViewById(R.id.no_task_TV);
        merchandiseList = view.findViewById(R.id.merchandise_list );
        totalTV = view.findViewById(R.id.total_TV);
        total = view.findViewById(R.id.total);
        addressTV = view.findViewById(R.id.address_TV);
        address = view.findViewById(R.id.address);
        timeTV = view.findViewById(R.id.time_TV);
        time = view.findViewById(R.id.time);
        remarkTV = view.findViewById(R.id.remark_TV);
        remark = view.findViewById(R.id.remark);
        taskTakerTV = view.findViewById(R.id.task_taker_TV);
        takerCard = view.findViewById(R.id.taker_card);
        selfie = view.findViewById(R.id.selfie);
        name = view.findViewById(R.id.name);
        account = view.findViewById(R.id.account);
        school = view.findViewById(R.id.school);
        checkPositionBnt = view.findViewById(R.id.check_taker_position_bnt);
        chatBnt = view.findViewById(R.id.chat_bnt);
        noTakerTV = view.findViewById(R.id.no_taker_TV);


        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        setTaskID();//taskID餵給聊天室
        detectOverTimeMyTask();//偵測任務過時，如果過時會提醒使用者
        setupRecyclerView();
        taskSonar();//偵測是否有創建任務，有就載入資訊

        return view;
    }

    private void setupRecyclerView() {

        orders = new ArrayList<>();
        merchandiseList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        orderAdapter = new OrderAdapter(orders , false);
        merchandiseList.setLayoutManager(layoutManager);
        merchandiseList.setAdapter(orderAdapter);

        //查看清單
        orderAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(!orderAdapter.getViewHolder().isItemLongClick()){//刪除鍵已出現
                    OrderCheckListActivity.setupList(orders.get(position).getMerchandiseList());
                    startActivity(new Intent(getContext() , OrderCheckListActivity.class));
                }
            }
        });
    }

    //偵測有無進行中的任務
    public void taskSonar(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){

                            allAppear();//出現元件
                            loadTaskInfo(documentSnapshot.getString("taskID"));

                            if(documentSnapshot.getBoolean("assigned")){
                                //載入接單者資料

                            }
                        }else {
                            allHide();
                        }
                    }
                });
    }

    public void loadTaskInfo(final String taskID){

        firestore.collection("task").document(taskID).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){

                            //載入任務資訊
                            total.setText("$ "+documentSnapshot.get("totalPrice" , Integer.TYPE));
                            address.setText(documentSnapshot.getString("address"));
                            time.setText(getTime(documentSnapshot.get("time" , Timestamp.class).toDate()));
                            remark.setText(documentSnapshot.getString("remark"));

                            //載入ItemOrder
                            loadItemOrder(taskID);
                        }
                    }
                });
    }

    //載入ItemOrder
    public void loadItemOrder(final String taskID){

        firestore.collection("task").document(taskID).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        //order
                        for(int i = 0 ; i < documentSnapshot.get("orderNum" , Integer.TYPE) ; i ++){

                            final int finalI = i;
                            firestore.collection("task").document(taskID).collection("order").document(String.valueOf(i))
                                    .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(final DocumentSnapshot orderDocumentSnapshot) {

                                    final ArrayList<ItemMerchandises> merchandises = new ArrayList<>();

                                    //merchandises
                                    for(int j = 0 ; j < orderDocumentSnapshot.get("merchandiseNum" , Integer.TYPE) ; j++){

                                        final int finalJ = j;
                                        firestore.collection("task").document(taskID).collection("order").document(String.valueOf(finalI))
                                                .collection("merchandises").document(String.valueOf(j)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot merchandiseDocumentSnapshot) {

                                                //add merchandises
                                                merchandises.add(new ItemMerchandises(merchandiseDocumentSnapshot.getString("merchandiseName") ,
                                                                                      merchandiseDocumentSnapshot.get("unitPrice" , Integer.TYPE) ,
                                                                                      merchandiseDocumentSnapshot.get("number" , Integer.TYPE)));
                                                //載入merchandises完畢
                                                if( finalJ ==  orderDocumentSnapshot.get("merchandiseNum" , Integer.TYPE) -1){
                                                    //add order
                                                    orderAdapter.addOrder(new ItemOrder(orderDocumentSnapshot.getString("storeName") ,
                                                            orderDocumentSnapshot.getString("detail") ,
                                                            merchandises ,
                                                            orderDocumentSnapshot.get("totalPrice" , Integer.TYPE)));
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
    }

    //載入接單者資料
    public void loadTaskTaker(){

        //載入接單者ID
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(final DocumentSnapshot myDocumentSnapshot) {
                if(myDocumentSnapshot.exists()){

                    takerCard.setVisibility(View.VISIBLE);

                    //load taskTaker info
                    firestore.collection("memberData").document(myDocumentSnapshot.getString("taskTaker")).get()
                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot takerDocumentSnapshot) {
                                    if(takerDocumentSnapshot.exists()){
                                        //大頭貼
                                        Picasso.get()
                                                .load(takerDocumentSnapshot.getString("selfiePath"))
                                                //圖片使用最低分辨率,降低使用空間大小
                                                .fit()
                                                .centerCrop()
                                                .into(selfie);

                                        name.setText(takerDocumentSnapshot.getString("name"));
                                        account.setText(takerDocumentSnapshot.getString("account"));
                                        school.setText(takerDocumentSnapshot.getString("school"));

                                        checkPositionBnt.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                //check position
                                            }
                                        });

                                        chatBnt.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                //chat
                                                taskID = myDocumentSnapshot.getString("taskID");
                                                startActivity(new Intent(getContext() , ChatRoomActivity.class));
                                            }
                                        });

                                    }
                                }
                            });
                }
            }
        });
    }

    //有任務出現元件
    public void allAppear(){

        noTaskTV.setVisibility(View.INVISIBLE);//無任務標籤

        merchandiseList.setVisibility(View.VISIBLE);
        totalTV.setVisibility(View.VISIBLE);
        total.setVisibility(View.VISIBLE);
        addressTV.setVisibility(View.VISIBLE);
        address.setVisibility(View.VISIBLE);
        timeTV.setVisibility(View.VISIBLE);
        time.setVisibility(View.VISIBLE);
        remarkTV.setVisibility(View.VISIBLE);
        remark.setVisibility(View.VISIBLE);
        taskTakerTV.setVisibility(View.VISIBLE);
        takerCard.setVisibility(View.INVISIBLE);//無接單者
        noTakerTV.setVisibility(View.VISIBLE);
    }

    //無任務全部隱藏
    public void allHide(){

          noTaskTV.setVisibility(View.VISIBLE);//無任務標籤

          merchandiseList.setVisibility(View.INVISIBLE);
          totalTV.setVisibility(View.INVISIBLE);
          total.setVisibility(View.INVISIBLE);
          addressTV.setVisibility(View.INVISIBLE);
          address.setVisibility(View.INVISIBLE);
          timeTV.setVisibility(View.INVISIBLE);
          time.setVisibility(View.INVISIBLE);
          remarkTV.setVisibility(View.INVISIBLE);
          remark.setVisibility(View.INVISIBLE);
          taskTakerTV.setVisibility(View.INVISIBLE);
          takerCard.setVisibility(View.INVISIBLE);
          noTakerTV.setVisibility(View.INVISIBLE);
    }

    //從timeStamp取得時間
    public String getTime(Date date){
        String result = splitString(splitString(date.toString() , " ")[3] , ":")[0]+":"+splitString(splitString(date.toString() , " ")[3] , ":")[1];
        return result;
    }

    //分割字串
    public String []splitString(String s , String regex){
        String[] split = s.split(regex);
        return split;
    }

    //偵測自己創的任務是否過時(依據task中自己的任務是否存在)
    public void detectOverTimeMyTask(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    firestore.collection("task").document(documentSnapshot.getString("taskID"))
                            .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(!documentSnapshot.exists()){//如果task文件不存在
                                //刪除myCurrentTask
                                firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                                        .document("task").delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        disableMatchTask();//停止媒合任務
                                        alertDialogPopUp();
                                        taskSonar();//重新設定布局
                                        Log.d("deleteMCT" , "success！");
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    //過時對話框
    public void alertDialogPopUp(){

        final android.support.v7.app.AlertDialog.Builder ADBuider = new AlertDialog.Builder(getContext());
        ADBuider.setTitle("目前任務");
        ADBuider.setMessage("您所創建的任務已經過時，已被自動刪除！");

        DialogInterface.OnClickListener ADClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case DialogInterface.BUTTON_POSITIVE:
                        detectOverTimeTaskAlertDialog.dismiss();
                        break;
                }
            }
        };

        ADBuider.setPositiveButton("確認" , ADClickListener);

        detectOverTimeTaskAlertDialog = ADBuider.create();
        detectOverTimeTaskAlertDialog.show();
    }

    public void disableMatchTask(){
        //任務刪除的話媒合任務關閉
        MainActivity.matchTimer.cancel();
        MainActivity.matchTimer = null;
        MainActivity.matchTask.cancel();
        MainActivity.matchTask = null;
        //重新建置
        MainActivity.buildMatchTask();
        //釋放監聽器的鎖
        MainActivity.MATCH_TASK_CODE = 0;
    }

    public void setTaskID(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    taskID = documentSnapshot.getString("taskID");
                }
            }
        });
    }
}

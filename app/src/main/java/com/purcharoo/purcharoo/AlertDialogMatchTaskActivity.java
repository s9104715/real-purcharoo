package com.purcharoo.purcharoo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class AlertDialogMatchTaskActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressBar progressBar;
    private int process = 120;//進度 max is 60*2 (每秒減2h4)
    private Timer timer;
    private TimerTask timerTask;
    private Button positiveBnt;
    private Button negativeBnt;
    private TextView msg;
    public static String taskID;//餵給聊天室
    //震動
    private Vibrator vib;

    //firestore
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;

    //processDialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_alert_dailog_match_task);

        progressBar = findViewById(R.id.match_task_PB);
        msg = findViewById(R.id.msg);
        positiveBnt = findViewById(R.id.positive_bnt);
        negativeBnt = findViewById(R.id.negative_bnt);

        positiveBnt.setOnClickListener(this);
        negativeBnt.setOnClickListener(this);

        //firebase初始化
        firestore = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();

        setTaskID();//taskID餵給聊天室
        shakeShake(new long[] {1000, 1000} , 0);
        setupProcessBar();
        buildCountDownTask();
        activateCountDownTask();
//        sendNotification();//發送通知給用戶

        //pop up method
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        getWindow().setLayout(1200, 920);//window size
    }

    @Override
    public void onBackPressed() {
        //do nothing
    }

    //倒數計時器，經過60秒倒數完成，提醒使用者配對失敗
    public void setupProcessBar() {

        progressBar.setProgress(process);//一開始max

    }

        public void buildCountDownTask (){

            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    process -= 1;//進度每秒減1
                    progressBar.setProgress(process);
                    if(process == 0){//進度 = 0

                        //釋放監聽器的鎖
                        MainActivity.MATCH_TASK_CODE = 0;

                        //修改ui需要runOnUiThread方法
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                msg.setText("訂單媒合失敗！，請點選 是 以重新等待接單");
                                msg.setTextColor(Color.RED);
                            }
                        });

                        //bnt更新新的點擊方法
                        positiveBnt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                        negativeBnt.setOnClickListener(null);

                        //終止震動
                        vib.cancel();

                        //修改資料庫contact = false ====> =拒絕接單者接單
                        Map<String,Object> contact = new HashMap<>();
                        contact.put("contact" , false);
                        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                                .document("task")
                                .set(contact , SetOptions.mergeFields("contact"));
                        //contacter變為空
                        Map<String,Object> contacter = new HashMap<>();
                        contacter.put("contacter" , "");
                        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                                .document("task")
                                .set(contacter , SetOptions.mergeFields("contacter"));

                        //終止任務
                        timer.cancel();
                        timer = null;
                        timerTask.cancel();
                        timerTask = null;
                    }
                }
            };
        }

        public void activateCountDownTask(){
            timer.schedule(timerTask , 100 , 500);//每秒減兩次 減120剛好一分鐘
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.positive_bnt:
                    //是的話進行媒合
                    assignTaskTaker();

                    //有人接單的話媒合任務關閉
                    MainActivity.matchTimer.cancel();
                    MainActivity.matchTimer = null;
                    MainActivity.matchTask.cancel();
                    MainActivity.matchTask = null;
                    //重新建置
                    MainActivity.buildMatchTask();
                    //釋放監聽器的鎖
                    MainActivity.MATCH_TASK_CODE = 0;

                    vib.cancel();
                    break;
                case R.id.negative_bnt:

                    //修改資料庫assigned = false ====> 拒絕接單者接單
                    Map<String,Object> contact = new HashMap<>();
                    contact.put("contact" , false);
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                            .document("task")
                            .set(contact , SetOptions.mergeFields("contact"));
                    //contacter變為空
                    Map<String,Object> contacter = new HashMap<>();
                    contacter.put("contacter" , "");
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                            .document("task")
                            .set(contacter , SetOptions.mergeFields("contacter"));

                    //讓MainActivity的timerTask可以透過監聽資料庫以開啟此Activity
                    MainActivity.MATCH_TASK_CODE = 0;

                    vib.cancel();

                    finish();
                    break;
            }
        }

    //震下去 patter long[]前數為幾秒後開始震動 ,後數為震動幾秒 ,repeat 0 為重複 -1為不重複
    public void shakeShake(long [] patter , int repeat){
        vib = (Vibrator) AlertDialogMatchTaskActivity.this.getSystemService(Service.VIBRATOR_SERVICE);
        vib.vibrate(patter, repeat);
    }

    //指派接單者
    public void assignTaskTaker(){

        //取得當前任務的id
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){

                    //依id進入task修改當前任務的assigned值為true
                    Map<String,Object> assigned = new HashMap<>();
                    assigned.put("assigned" , true);
                    firestore.collection("task").document(documentSnapshot.getString("taskID"))
                            .set(assigned , SetOptions.mergeFields("assigned"));
                    //myCurrentTask的assigned值為true
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                            .document("task").set(assigned , SetOptions.mergeFields("assigned"));
                }
            }
        });

        matchDialog();
        setProgressTime(progressDialog , 3000);//3秒後跳轉到接單者資訊
    }

    //媒合 process dialog
    private void matchDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("載入對方資訊");
        progressDialog.setMessage("媒合中.....");
        progressDialog.show();
    }

    public void setProgressTime(final ProgressDialog progressDialog , int delayMillis){

        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {

                //媒合成功 在task和myCurrentTask裡新增接單者UID
                //從欄位contacter取得當前連絡者
                firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                        .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){

                            String contacter = documentSnapshot.getString("contacter");

//                            Map<String,Object> upLoadTaskTaker = new HashMap<>();
//                            upLoadTaskTaker.put("taskTaker" , documentSnapshot.getString("contacter"));

                            //新增task的taskTaker欄位
                            firestore.collection("task").document(documentSnapshot.getString("taskID"))
                                    .update("taskTaker" , contacter);
                            //新增myCurrentTask的taskTaker欄位
                            firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                                    .document("task").update("taskTaker" , contacter);

                            createChatRoom(documentSnapshot.getString("taskID"));//創建聊天室

                            //delete myCurrentTask的contacter
                            Map<String,Object> deleteContacter = new HashMap<>();
                            deleteContacter .put("contacter" , FieldValue.delete());
                            firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                                    .document("task").update(deleteContacter);
                        }
                    }
                });

                progressDialog.dismiss();
                //跳轉到接單者資訊及查看其所在位置
                startActivity(new Intent(getApplicationContext() , ChatRoomActivity.class));
                finish();
                taskID = null;//clear
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, delayMillis);//毫秒計算
    }

//    //傳送系統通知給用戶
//    public void sendNotification(){
//
//        // 取得NotificationManager物件
//        NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//        // 建立NotificationCompat.Builder物件
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//        builder.setVibrate(new long[] {0 , 1000});//設定震動 1秒
//        // 設定內容
//        builder.setSmallIcon(R.drawable.ic_menu_camera)
//                .setTicker("Purcharoo訂單")
//                .setWhen(System.currentTimeMillis())
//                .setContentTitle("Purcharoo訂單")
//                .setContentText("有人想要接下您創建的任務！");
//        // 建立通知物件
//        Notification notification = builder.build();
//        // 使用CUSTOM_EFFECT_ID為編號發出通知
//        manager.notify(1 , notification);
//        shakeShake(new long[] {500 , 1000} , -1);//震動
//    }

    public void setTaskID(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                .document("task").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.exists()){
                    taskID = documentSnapshot.getString("taskID");
                }
            }
        });
    }

    //建立聊天室
    public void createChatRoom(final String taskID){

        firestore.collection("task").document(taskID).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){

                            Map<String , Object> chatRoomInfo = new HashMap<>();
                            chatRoomInfo.put("userID" , documentSnapshot.getString("userID"));//訂單者
                            chatRoomInfo.put("taskTaker" , documentSnapshot.getString("taskTaker"));//接單者
                            chatRoomInfo.put("lineNum" , 0);//聊天行數
                            firestore.collection("task").document(taskID).collection("chatRoom").document("room")
                                    .set(chatRoomInfo).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("createChatRoom" , "success！");
                                }
                            });
                        }
                    }
                });
    }
}



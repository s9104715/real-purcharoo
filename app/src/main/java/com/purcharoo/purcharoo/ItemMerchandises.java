package com.purcharoo.purcharoo;

public class ItemMerchandises {

    private String merchandiseName;
    private int unitPrice;
    private int number;
    private int totalPrice;

    public ItemMerchandises() {
    }

    public ItemMerchandises(String merchandiseName, int unitPrice, int number) {
        this.merchandiseName = merchandiseName;
        this.unitPrice = unitPrice;
        this.number = number;
        calTotalPrice();
    }

    public String getMerchandiseName() {
        return merchandiseName;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public int getNumber() {
        return number;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setMerchandiseName(String merchandiseName) {
        this.merchandiseName = merchandiseName;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void calTotalPrice(){
        setTotalPrice(unitPrice * number);
    }
}

package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.squareup.picasso.Picasso;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //頂層和底部toolbar元件
    private Toolbar topToolbar;
    private BottomNavigationView botToolBar;
    //fragment元件
    private FragmentMainPage mainPage;
    private FragmentTask task;
    private FragmentShare share;
    private FragmentSetting setting;
    private int FRAGMENT_INDEX = 0;//紀錄滑到哪一頁
    //firestore
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    //會員資料
    private static ImageView selfie;
    private static TextView name;
    private static TextView account;

    //訂單媒合任務 給訂單相關的活動使用(當接單者要求接下任務時，訂單者訊息跳出，並選擇是否接受)
    public static Timer matchTimer;
    public static TimerTask matchTask;
    public static int MATCH_TASK_CODE = 0; // 0 開放監聽任務 1 for鎖定監聽任務(開不起來AlertDialog)
    public static Thread startMatchTaskActivityThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //預設Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container , new FragmentMainPage()).commit();

        //頂層toolbar設置
        topToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(topToolbar);

        //底層toolbar設置
        setupMainBotToolbar();

        setupNavigationView(topToolbar);//側選單設置

        setupMatchTaskStarter();//訂單媒合視窗開啟的建置
        buildMatchTask();//訂單媒合任務建置
        initAppMatchTask();//app開啟後如果有進行中的任務，即開啟任務監聽器
    }

    //側選單返回指令設定
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_page_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //頂層toolbar功能
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_tool_bar, menu);

        //初始化FragmentSetting以防止從searchActivity中進入AccSetting的updateSelfie方法錯誤，並返回MainFragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container ,setting).commit();//SettingFragment
        FragmentManager mainFragmentManager = getSupportFragmentManager();
        FragmentTransaction maintransaction = mainFragmentManager.beginTransaction();
        maintransaction.replace(R.id.main_container , new FragmentMainPage()).commit();

        MenuItem searchItem = menu.findItem(R.id.search_bar);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        //進入搜尋頁面
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this , SearchActivity.class));
                searchView.clearFocus();//收起鍵盤
                searchView.onActionViewCollapsed();//收起searchview
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    //側選單功能
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_acc_manage) {

            //初始化FragmentSetting以防止AccSetting的updateSelfie方法錯誤，並返回MainFragment
            if(FRAGMENT_INDEX == 0){
                initializeFragmentSetting();
                FragmentManager mainFragmentManager = getSupportFragmentManager();
                FragmentTransaction maintransaction = mainFragmentManager.beginTransaction();
                maintransaction.replace(R.id.main_container , new FragmentMainPage()).commit();
            }else if(FRAGMENT_INDEX == 1){
                initializeFragmentSetting();
                FragmentManager taskFragmentManager = getSupportFragmentManager();
                FragmentTransaction tasktransaction = taskFragmentManager.beginTransaction();
                tasktransaction.replace(R.id.main_container , new FragmentTask()).commit();
            }else if(FRAGMENT_INDEX == 2){
                initializeFragmentSetting();
                FragmentManager shareFragmentManager = getSupportFragmentManager();
                FragmentTransaction sharetransaction = shareFragmentManager.beginTransaction();
                sharetransaction.replace(R.id.main_container , new FragmentShare()).commit();
            }
            //進入AccSetting
            startActivity(new Intent(this , AccSettingActivity.class));

        } else if (id == R.id.nav_wallet) {
            Intent wallet = new Intent(MainActivity.this, WalletActivity.class);
            startActivity(wallet);

        } else if (id == R.id.nav_record) {

        } else if (id == R.id.nav_rate) {

        } else if (id == R.id.nav_loc_manage) {

            //進入SetPositionActivity
            startActivity(new Intent(this , SetPositionActivity.class));

        } else if (id == R.id.nav_story) {

        }else if(id == R.id.nav_sign_out){
            //isLogin還原為false
            Map<String,Object> changedata = new HashMap<>();
            changedata.put("isLogin" , false);
            firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                    .set(changedata , SetOptions.mergeFields("isLogin"));
            //登出
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(this , LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_page_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //主頁底部toolbar設置
    //sample
    public void setupMainBotToolbar(){
        mainPage = new FragmentMainPage();
        task = new FragmentTask();
        share = new FragmentShare();
        setting = new FragmentSetting();
        botToolBar = (BottomNavigationView)findViewById(R.id.main_bottom_toolbar);
        botToolBar.setSelectedItemId(R.id.example1);//主頁已點擊，預設highlight
        botToolBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                switch (menuItem.getItemId()){
                    case R.id.example1:
                        transaction.replace(R.id.main_container , mainPage).commit();//進入MainPageFragment
                        FRAGMENT_INDEX = 0;
                        return true;
                    case R.id.example2:
                        transaction.replace(R.id.main_container , task).commit();//進入TaskFragment
                        FRAGMENT_INDEX = 1;
                        return true;
                    case R.id.example3:
                        transaction.replace(R.id.main_container , share).commit();//進入ShareFragment
                        FRAGMENT_INDEX = 2;
                        break;
                    case R.id.example4:
                        transaction.replace(R.id.main_container ,setting).commit();//SettingFragment
                        FRAGMENT_INDEX = 3;
                        break;
                }
                return true;
            }
        });
    }

    //側選單設置、讀取資料庫
    public void setupNavigationView(Toolbar toolbar){
        //nav
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_page_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.main_page_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //firestore
        if(navigationView.getHeaderCount() > 0){
            View header = navigationView.getHeaderView(0);//取得nav的header及其物件

            selfie = (ImageView)header.findViewById(R.id.nav_selfie);
            name = (TextView)header.findViewById(R.id.nav_name);
            account = (TextView)header.findViewById(R.id.nav_account);

            selfie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), CheckSelfieActivity.class));
                }
            });

            auth = FirebaseAuth.getInstance();
            firestore = FirebaseFirestore.getInstance();
            //處理帳號資料
            firestore.collection("memberData").document( auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            name.setText(documentSnapshot.getString("name"));
                            account.setText(auth.getCurrentUser().getEmail());
                            Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                    //圖片使用最低分辨率,降低使用空間大小
                                    .fit()
                                    .centerCrop()
                                    .into(selfie);//取得大頭貼

                            //已登入值設為true
                            Map<String,Object> changedata = new HashMap<>();
                            changedata.put("isLogin" , true);
                            firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                                    .set(changedata , SetOptions.mergeFields("isLogin"));
                        }
                    }
                });
        }
    }

    //提供給AccSettingActivity更新測選單資料
    public static void update(final String s){
        firestore.collection("memberData").document( auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            name.setText(documentSnapshot.getString("name"));
                            account.setText(auth.getCurrentUser().getEmail());
                            Picasso.get().load(s)
                                    //圖片使用最低分辨率,降低使用空間大小
                                    .fit()
                                    .centerCrop()
                                    .into(selfie);//取得大頭貼
                        }
                    }
                });
        }

        public void initializeFragmentSetting(){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container ,setting).commit();//SettingFragment
        }

        /*
        訂單媒合任務
        setupMatchTaskStarter : 負責開啟類AD的activity,執行一次即中斷,buildMatchTask如果沒鎖住會執行此任務
        buildMatchTask : 媒合任務監聽器,負責監聽有無接單者接下此單，接下此單後,MATCH_TASK_CODE會將此任務鎖住
        activateMatchTask : 啟動buildMatchTask
         */
        //開啟媒合視窗的任務
        public void setupMatchTaskStarter(){

            startMatchTaskActivityThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(MainActivity.this , AlertDialogMatchTaskActivity.class));
                    startMatchTaskActivityThread.interrupt();
                }
            });
        }

        public static void buildMatchTask(){

            matchTimer = new Timer();
            matchTask = new TimerTask() {
                @Override
                public void run() {

                    Log.d("msg" , "ssss");
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                            .document("task").get()
                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if(documentSnapshot.exists()){
                                        if (documentSnapshot.getBoolean("contact")){
                                            if(MATCH_TASK_CODE == 0){

                                                startMatchTaskActivityThread.run();//媒合任務開始

                                                //防止因為繼續監聽資料庫而開啟複數的AlertDialogMatchTaskActivity
                                                MATCH_TASK_CODE = 1;
                                            }
                                        }
                                    }
                                }
                            });
                    }
                };
            }

        //啟動媒合任務監聽器
        public static void activateMatchTask(){
            matchTimer.schedule(matchTask, 0 , 5000);
        }

        //app開啟後如果有進行中的任務，即開啟任務監聽器
        public void initAppMatchTask(){

            firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                    .document("task").get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                if(!documentSnapshot.getBoolean("contact")){
                                    activateMatchTask();
                                }
                            }
                        }
                    });
        }
    }




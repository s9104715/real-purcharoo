package com.purcharoo.purcharoo;

import java.util.Date;

public class ItemArticle {

    private String userUID;
    private String articleName;
    private String accout;
    private Date date;
    private String articleCode;//文章碼

    public ItemArticle(String userUID, String articleName, String accout , Date date , String articleCode) {
        this.userUID = userUID;
        this.articleName = articleName;
        this.accout = accout;
        this.date = date;
        this.articleCode = articleCode;
    }

    public String getUserUID() {
        return userUID;
    }

    public String getArticleName() {
        return articleName;
    }

    public String getAccout() {
        return accout;
    }

    public Date getDate() {
        return date;
    }

    public String getArticleCode() {
        return articleCode;
    }
}

package com.purcharoo.purcharoo;

public class ItemSearch {
    private String storePicUri;//商家照片uri
    private String storeText;//店名

    public ItemSearch(String storePicUri, String storeText){
        this.storePicUri = storePicUri;
        this.storeText = storeText;
    }

    public String getStorePicUri() {
        return storePicUri;
    }

    public String getStoreText() {
        return storeText;
    }

}
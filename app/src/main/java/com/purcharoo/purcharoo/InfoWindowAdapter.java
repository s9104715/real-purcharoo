package com.purcharoo.purcharoo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

    public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View mWindow;
        private Context context;

        public InfoWindowAdapter(Context context) {
            this.context = context;
            mWindow = LayoutInflater.from(context).inflate(R.layout.marker_info_window,null);
        }

        private void rendowWindowText(Marker marker, View view) {
            String title = marker.getTitle();
            TextView tvTitle = (TextView) view.findViewById(R.id.title);
            if (!title.equals("")) {
                tvTitle.setText(title);
            }
            String snippet = marker.getSnippet();
            TextView tvSnippet = (TextView) view.findViewById(R.id.snippet);
            if (!snippet.equals("")) {
                tvSnippet.setText(snippet);
            }

        }

        @Override
        public View getInfoWindow(Marker marker) {
            rendowWindowText(marker,mWindow);
            return mWindow;
        }

        @Override
        public View getInfoContents(Marker marker) {
            rendowWindowText(marker,mWindow);
            return mWindow;
        }

        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }
    }



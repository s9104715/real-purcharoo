package com.purcharoo.purcharoo;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                //碰觸地圖
                FragmentMainPage.scrollView.requestDisallowInterceptTouchEvent(true);//FragmentMainPage的scrollView不能滑動
                break;
            case MotionEvent.ACTION_UP:
                //未碰觸地圖
                FragmentMainPage.scrollView.requestDisallowInterceptTouchEvent(false);//FragmentMainPage的scrollView可以滑動
                break;
        }
        return super.dispatchTouchEvent(event);
    }
}

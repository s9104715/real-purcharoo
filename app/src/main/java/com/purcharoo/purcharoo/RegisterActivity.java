package com.purcharoo.purcharoo;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.paperdb.Paper;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText nameET , mailET , passET , passComET , verCodeET;
    private Spinner schSpinner;
    private RadioGroup genderRG;
    private RadioButton genderRB;
    private Button registerBnt , sendCodeBnt;
    private TextView haveAccoutTV;
    private String verCode = null;

    //Firebase
    FirebaseAuth auth;
    FirebaseFirestore firestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_register);

        nameET = findViewById(R.id.et_name);
        mailET = findViewById(R.id.et_email_register);
        passET = findViewById(R.id.et_password_register);
        passComET = findViewById(R.id.et_password_comfirm);
        schSpinner = findViewById(R.id.sch_spinner);
        genderRG = findViewById(R.id.rg_gender);
        registerBnt = findViewById(R.id.btn_register);

        haveAccoutTV = findViewById(R.id.tv_have_account);
        //觸及時文字變紅增加底線、沒觸及則變回default
        haveAccoutTV.setOnTouchListener(new View.OnTouchListener() {
            int defaultColor = haveAccoutTV.getCurrentTextColor();
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        haveAccoutTV.setTextColor(Color.RED);
                        haveAccoutTV.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                        break;
                    case MotionEvent.ACTION_UP:
                        haveAccoutTV.setTextColor(defaultColor);
                        haveAccoutTV.setPaintFlags(0);
                        RegisterActivity.super.onBackPressed();//回前頁
                        break;
                }
                return true;
            }
        });

        verCodeET = findViewById(R.id.et_verify_code);
        sendCodeBnt = findViewById(R.id.send_code_bnt);
        sendCodeBnt.setOnClickListener(this);

        setupSchSpinner();
        registerBnt.setOnClickListener(this);

    }

    public void register(){

        final String name = nameET.getText().toString().trim();
        final String school = schSpinner.getSelectedItem().toString().trim();

        //取得性別String
        int getRGId = genderRG.getCheckedRadioButtonId();
        genderRB = (RadioButton)findViewById(getRGId);
        final String gender = (String) genderRB.getText();

        final int wallet = 0;
        final double rate = 0;
        //default selfie
        final String selfiePath = "https://firebasestorage.googleapis.com/v0/b/purcharoo.appspot.com/o/memberSelfie%2Fdefault.png?alt=media&token=d22d7087-96ab-4004-93cc-0dcfa2349f18";
        final String mail = mailET.getText().toString().trim();
        String pass = passET.getText().toString().trim();
        String passCom = passComET.getText().toString().trim();
        String userVerCode = verCodeET.getText().toString().trim();

        if(name.isEmpty()){
            nameET.setError("姓名不能為空");
            nameET.requestFocus();
            return;
        }
        if(school.isEmpty()){
            Toast.makeText(getApplicationContext() , "學校尚未選擇" , Toast.LENGTH_SHORT).show();
            schSpinner.requestFocus();
            return;
        }
        if(gender.isEmpty()){
            Toast.makeText(getApplicationContext() , "請選擇性別" , Toast.LENGTH_SHORT).show();
            genderRG.requestFocus();
            return;
        }
        if(mail.isEmpty()){
            mailET.setError("學校信箱不能為空");
            mailET.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(mail).matches()){
            mailET.setError("信箱格式不正確");
            mailET.requestFocus();
            return;
        }
        //目前僅有輔仁大學信箱
        if(!mail.contains("mail.fju.edu.tw")){
            mailET.setError("需要學校信箱");
            mailET.requestFocus();
            return;
        }
        if(userVerCode.isEmpty()){
            verCodeET.setError("請輸入驗證碼");
            verCodeET.requestFocus();
            return;
        }
        if(!userVerCode.equals(verCode)){
            verCodeET.setError("驗證碼不正確");
            verCodeET.requestFocus();
            return;
        }
        if(pass.isEmpty()){
            passET.setError("密碼不能為空");
            passET.requestFocus();
            return;
        }
        if(pass.length() < 8){
            passET.setError("密碼長度必須大於8位");
            passET.requestFocus();
            return;
        }
        if(passCom.isEmpty()){
            passComET.setError("請確認密碼");
            passComET.requestFocus();
            return;
        }
        if(!pass.equals(passCom)){
            passComET.setError("請重新確認密碼");
            passComET.requestFocus();
            return;
        }

        auth = FirebaseAuth.getInstance();
        //創建帳號
        auth.createUserWithEmailAndPassword(mail , pass)
                .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    //新增帳號的資料
                    FirebaseUser firebaseUser = task.getResult().getUser();
                    User user = new User(name , mail ,  school , gender , wallet , rate , selfiePath);
                    firestore.collection("memberData").document(firebaseUser.getUid())
                            .set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //註冊完使用者馬上進入登入介面登入
                            FirebaseAuth.getInstance().signOut();
                            startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                            finish();
                        }
                    });
                    Toast.makeText(getApplicationContext() , "註冊成功" , Toast.LENGTH_SHORT).show();
                }else{
                    if(task.getException().toString().contains("The email address is already in use")){
                        mailET.setError("帳號已存在");
                        mailET.requestFocus();
                    }else{
                        Toast.makeText(getApplicationContext(), "註冊失敗"+task.getException().toString() , Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    //firebase帶入學校下拉式選單資料
    public void setupSchSpinner(){

        firestore = FirebaseFirestore.getInstance();
        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                List<String> schList = new ArrayList<>();
                for(DocumentSnapshot snapshot : queryDocumentSnapshots){
                    schList.add(snapshot.getString("name"));
                }
                    ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext() , R.layout.spinner_item ,schList);
                    spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    schSpinner.setAdapter(spinnerAdapter);//載入學校清單
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
                register();
                break;
            case R.id.send_code_bnt:
                verCode = randomCode();
                sendEmail(verCode);
                break;
        }
    }

    public void sendEmail(String verCode){

      if(mailET.getText().toString().trim().isEmpty()){
          mailET.setError("學校信箱不能為空");
          mailET.requestFocus();
          return;
      }
        if(!Patterns.EMAIL_ADDRESS.matcher(mailET.getText().toString().trim()).matches()){
            mailET.setError("信箱格式不正確");
            mailET.requestFocus();
            return;
        }
        //目前僅有輔仁大學信箱
        if(!mailET.getText().toString().trim().contains("mail.fju.edu.tw")){
            mailET.setError("需要學校信箱");
            mailET.requestFocus();
            return;
        }
      MailSender sender = new MailSender(this , mailET.getText().toString().trim() , "您的Purcharoo驗證碼" , "您的驗證碼為："+verCode);
      sender.execute();//send email

    }

    //產生驗證碼
    public String randomCode(){
        final String DATA= "0123456789";
        Random random = new Random();
        StringBuilder code = new StringBuilder(4);

        for(int i = 0 ; i < code.capacity() ; i++){
            code.append(DATA.charAt(random.nextInt(DATA.length())));
        }
        return code.toString();
    }
}

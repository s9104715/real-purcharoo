package com.purcharoo.purcharoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;


public class OrderFinalCheckoutActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView total;
    private TextView address;
    private TextView time;
    private TextView remark;
    private Button confirmBnt;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private OrderAdapter orderAdapter;

    //processDialog
    private ProgressDialog progressDialog;

    //Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_order_final_checkout);

        recyclerView = findViewById(R.id.goods_list);
        total = findViewById(R.id.total_price);
        address = findViewById(R.id.tv_address);
        time = findViewById(R.id.tv_time);
        remark = findViewById(R.id.remark);
        confirmBnt = findViewById(R.id.confirm_bnt);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        confirmBnt.setOnClickListener(this);

        setupTopToolBar();
        setupDetail();
        setupRecyclerView();
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.order_final_checkout_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void setupDetail(){
        total.setText(OrderFragmentOrders.total.getText());
        time.setText(OrderFragmentOrders.timeET.getText());
        remark.setText(OrderFragmentOrders.remarkET.getText());
        address.setText(OrderMapActivity.selectedAddress);
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(OrderFinalCheckoutActivity.this);
        orderAdapter = new OrderAdapter(OrderFragmentOrders.orders , false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);

        //查看清單
        orderAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(!orderAdapter.getViewHolder().isItemLongClick()){//刪除鍵已出現
                    OrderCheckListActivity.setupList(OrderFragmentOrders.orders.get(position).getMerchandiseList());
                    startActivity(new Intent(OrderFinalCheckoutActivity.this , OrderCheckListActivity.class));
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirm_bnt:

                //建立任務
                createTask();

                break;
        }
    }

    //資料庫創建任務
    public void createTask(){

        createTaskDialog();//開始process dialog

        //創造任務
        final String randomCode = randomCode(12);
        Map<String,Object> task = new HashMap<>();
        task.put("taskID" , randomCode);
        task.put("userID" , auth.getCurrentUser().getUid());
        task.put("address" , address.getText().toString());
        task.put("latitude" , OrderMapActivity.latitude);
        task.put("longitude" , OrderMapActivity.longitude);
        task.put("totalPrice" , Integer.valueOf(splitString(total.getText().toString() , " ")[1]));
        task.put("time" ,  getTimeStamp(time.getText().toString()));
        task.put("remark" , remark.getText().toString());
        task.put("assigned" , false);//是否已有接單者
        task.put("orderNum" , OrderFragmentOrders.orders.size());
        firestore.collection("task").document(randomCode)
                .set(task)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                //create Order
                createOrder(randomCode , OrderFragmentOrders.orders );

                createMyCurrentTask(randomCode);

                //如果任務建立成功 3秒後跳回主業並啟動媒合任務
                setProgressTime(progressDialog , 3000);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext() , " 任務建立失敗" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    //建立任務 process dialog
    private void createTaskDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("建立任務");
        progressDialog.setMessage("建立中.....");
        progressDialog.show();
    }

    public void setProgressTime(final ProgressDialog progressDialog , int delayMillis){

        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();

                //啟動媒合任務
                MainActivity.activateMatchTask();

                //跳回主頁
                OrderFinalCheckoutActivity.super.finish();
                if(OrderMapActivity.instance != null && OrderActivity.instance != null) {
                    OrderMapActivity.instance.finish();
                    OrderActivity.instance.finish();
                }
                Toast.makeText(getApplicationContext() , "任務建立成功" , Toast.LENGTH_SHORT).show();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, delayMillis);//毫秒計算
    }

    //會員資料中設置目前任務
    public void createMyCurrentTask(String randomCode){

        Map<String,Object> myCurrentTask = new HashMap<>();
        myCurrentTask.put("taskID" , randomCode);
        myCurrentTask.put("time" ,  getTimeStamp(time.getText().toString()));
        myCurrentTask.put("assigned" , false);
        myCurrentTask.put("contact" , false);//是否有無接單者想要聯繫你
        myCurrentTask.put("contacter" , "");//聯絡者
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask").document("task")
                .set(myCurrentTask);
    }

    public void createOrder(String randomCode , List<ItemOrder> orders){

        //create order
        for(int i = 0 ; i < orders.size(); i ++){

            Map<String,Object> order = new HashMap<>();
            order.put("storeName" , orders.get(i).getStoreName());//店名
            order.put("totalPrice" , orders.get(i).getTotalPrice());//單order總價
            order.put("merchandiseNum" , orders.get(i).getMerchandiseList().size());//商品種類的數量
            order.put("detail" , orders.get(i).getDetail());//細目
            firestore.collection("task").document(randomCode).collection("order").document(String.valueOf(i))
                    .set(order);

            //create merchandises
            for(int j = 0 ; j < orders.get(i).getMerchandiseList().size() ; j ++){

                Map<String,Object> merchandises = new HashMap<>();
                merchandises.put("merchandiseName" , orders.get(i).getMerchandiseList().get(j).getMerchandiseName());//品名
                merchandises.put("unitPrice" , orders.get(i).getMerchandiseList().get(j).getUnitPrice());//商品單價
                merchandises.put("number" , orders.get(i).getMerchandiseList().get(j).getNumber());//商品數量
                merchandises.put("totalPrice" , orders.get(i).getMerchandiseList().get(j).getTotalPrice());//數量*單價
                firestore.collection("task").document(randomCode).collection("order").document(String.valueOf(i))
                        .collection("merchandises").document(String.valueOf(j))
                        .set(merchandises);
            }
        }
    }

    //產生隨機名稱
    public String randomCode(int capacity){
        final String DATA= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder code = new StringBuilder(capacity);

        for(int i = 0 ; i < code.capacity() ; i++){
            code.append(DATA.charAt(random.nextInt(DATA.length())));
        }
        return code.toString();
    }

    //分割字串
    public String []splitString(String s , String regex){
        String[] split = s.split(regex);
        return split;
    }

    //取得時間
    public java.sql.Timestamp getTimeStamp(String time){

        Date d = new Date();
        d.setHours(Integer.valueOf(splitString(time , ":")[0]));//小時
        d.setMinutes(Integer.valueOf(splitString(time , ":")[1]));//分鐘
        d.setSeconds(0);
        java.sql.Timestamp timestamp = new java.sql.Timestamp(d.getTime());
        return timestamp;
    }
}

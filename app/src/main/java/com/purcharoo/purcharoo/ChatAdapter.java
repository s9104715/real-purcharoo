package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import at.markushi.ui.CircleButton;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ExampleViewHolder> {

    private List<ItemChat> list;//清單
    private ExampleViewHolder viewHolder;

    //firebase
    private FirebaseFirestore firestore;
    private FirebaseAuth auth;

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入order_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        //對方的對話
        private CircleImageView otherSelfie;
        private TextView otherText;
        private TextView otherTime;
        //自己的對話
        private CircleImageView mySelfie;
        private TextView myText;
        private TextView myTime;

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener) {
            super(itemView);

            otherSelfie = itemView.findViewById(R.id.other_selfie);
            otherText = itemView.findViewById(R.id.other_text);
            otherTime = itemView.findViewById(R.id.other_time);
            mySelfie = itemView.findViewById(R.id.my_selfie);
            myText = itemView.findViewById(R.id.my_text);
            myTime = itemView.findViewById(R.id.my_line);

            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public ChatAdapter(List<ItemChat> list) {
        this.list = list;
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
    }

    //Adapter載入order_item.xml方法
    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat,
                parent, false);
        viewHolder= new ExampleViewHolder(v, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ExampleViewHolder holder, final int position) {

        final ItemChat currentItem = list.get(position);

        if(currentItem.getUserUID().equals(auth.getCurrentUser().getUid())){//符合自己的帳號，因此判定為自己的對話

            //隱藏對方的物件
            holder.otherSelfie.setVisibility(View.INVISIBLE);
            holder.otherText.setVisibility(View.INVISIBLE);
            holder.otherText.setVisibility(View.INVISIBLE);

            //載入自己的物件
            firestore.collection("memberData").document(currentItem.getUserUID()).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                //firebase設置大頭貼
                                Picasso.get()
                                        .load(documentSnapshot.getString("selfiePath"))
                                        //圖片使用最低分辨率,降低使用空間大小
                                        .fit()
                                        .centerCrop()
                                        .into(holder.mySelfie);
                            }
                        }
                    });
            holder.myText.setText(currentItem.getText());
            holder.myTime.setText(currentItem.getTime());
        }else {

            //隱藏自己的物件
            holder.mySelfie.setVisibility(View.INVISIBLE);
            holder.myText.setVisibility(View.INVISIBLE);
            holder.myTime.setVisibility(View.INVISIBLE);

            //載入對方物件
            firestore.collection("memberData").document(currentItem.getUserUID()).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                //firebase設置大頭貼
                                Picasso.get()
                                        .load(documentSnapshot.getString("selfiePath"))
                                        //圖片使用最低分辨率,降低使用空間大小
                                        .fit()
                                        .centerCrop()
                                        .into(holder.otherSelfie);
                            }
                        }
                    });
            holder.otherText.setText(currentItem.getText());
            holder.otherTime.setText(currentItem.getTime());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //新增item
    public void addMsg(ItemChat itemChat){
        list.add(itemChat);
        notifyDataSetChanged();
    }

    public List<ItemChat> getList() {
        return list;
    }

    public ExampleViewHolder getViewHolder() {
        return viewHolder;
    }
}

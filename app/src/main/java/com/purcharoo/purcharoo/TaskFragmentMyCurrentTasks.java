package com.purcharoo.purcharoo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;


/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragmentMyCurrentTasks extends Fragment {

    private List<ItemTask> tasks;
    private RecyclerView recyclerView;
    private TaskAdapter taskAdapter;
    private RecyclerView.LayoutManager layoutManager;
    public static ItemTask itemTask;//餵給taskInfo的資料
    private TextView noTaskTV;
    final double [] coordinates = new double[2];//兩筆資料,前為latitude 後為longitude

    //firestore
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;

    //位置權限
    private static final int REQUEST_LOCATION = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_my_current_task, container, false);

        recyclerView = view.findViewById(R.id.list);
        noTaskTV = view.findViewById(R.id.no_task_TV);
        tasks = new ArrayList<>();

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        setCurrentLocation();
        setupRecyclerView();
        loadTaskTaking();

        return view;
    }

    private void setupRecyclerView() {

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        taskAdapter = new TaskAdapter(tasks);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(taskAdapter);

        //查看任務
        taskAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //need position
                itemTask = taskAdapter.getList().get(position);
                startActivity(new Intent(getContext() , TaskInfoActivity.class));
            }
        });
    }

    //載入接單
    public void loadTaskTaking(){

        final ArrayList<String> taskIDList = new ArrayList<>();
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("taskTaking")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            taskIDList.add(documentSnapshot.getId());
                        }
                        if(taskIDList.size() == 0){//無任務
                            noTaskTV.setVisibility(View.VISIBLE);
                        }else {
                            firestore.collection("task").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                                        for(int i = 0 ; i < taskIDList.size() ; i++){
                                            if(documentSnapshot.getId().equals(taskIDList.get(i))){
                                                //載入接單
                                                loadItemOrder(documentSnapshot.getId());
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
    }

    //載入ItemOrder
    public void loadItemOrder(final String taskID){

        firestore.collection("task").document(taskID).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(final DocumentSnapshot taskDocumentSnapshot) {

                        final ArrayList <ItemOrder> orders = new ArrayList<>();

                        //order
                        for(int i = 0 ; i < taskDocumentSnapshot.get("orderNum" , Integer.TYPE) ; i ++){

                            final int finalI = i;
                            firestore.collection("task").document(taskID).collection("order").document(String.valueOf(i))
                                    .get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(final DocumentSnapshot orderDocumentSnapshot) {

                                    final ArrayList<ItemMerchandises> merchandises = new ArrayList<>();

                                    //merchandises
                                    for(int j = 0 ; j < orderDocumentSnapshot.get("merchandiseNum" , Integer.TYPE) ; j++){

                                        final int finalJ = j;
                                        firestore.collection("task").document(taskID).collection("order").document(String.valueOf(finalI))
                                                .collection("merchandises").document(String.valueOf(j)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot merchandiseDocumentSnapshot) {

                                                //add merchandises
                                                merchandises.add(new ItemMerchandises(merchandiseDocumentSnapshot.getString("merchandiseName") ,
                                                        merchandiseDocumentSnapshot.get("unitPrice" , Integer.TYPE) ,
                                                        merchandiseDocumentSnapshot.get("number" , Integer.TYPE)));

                                                //載入merchandises完畢
                                                if( finalJ ==  orderDocumentSnapshot.get("merchandiseNum" , Integer.TYPE) -1){

                                                    //add order
                                                    orders.add(new ItemOrder(orderDocumentSnapshot.getString("storeName") ,
                                                            orderDocumentSnapshot.getString("detail") ,
                                                            merchandises ,
                                                            orderDocumentSnapshot.get("totalPrice" , Integer.TYPE)));

                                                    //載入order完畢
                                                    if( finalI == taskDocumentSnapshot.get("orderNum" , Integer.TYPE) -1){
                                                        //載入itemTask
                                                        //如果是自己的任務就不要載入
                                                        if(!taskDocumentSnapshot.getString("userID").equals(auth.getCurrentUser().getUid())){
                                                            if(!taskDocumentSnapshot.getBoolean("assigned")){
                                                                taskAdapter.addTask(new ItemTask(taskDocumentSnapshot.getString("taskID") ,
                                                                        taskDocumentSnapshot.getString("userID") ,
                                                                        taskDocumentSnapshot.getString("address") ,
                                                                        taskDocumentSnapshot.getDouble("latitude") ,
                                                                        taskDocumentSnapshot.getDouble("longitude") ,
                                                                        coordinates ,
                                                                        taskDocumentSnapshot.get("totalPrice" , Integer.TYPE) ,
                                                                        getTime(taskDocumentSnapshot.get("time", Timestamp.class).toDate()) ,
                                                                        orders ,
                                                                        taskDocumentSnapshot.getString("remark") ,
                                                                        taskDocumentSnapshot.getBoolean("assigned")));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
    }

    public void setCurrentLocation(){

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //提醒使用者開啟位置權限
            ActivityCompat.requestPermissions(getActivity() , new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }else {
            FragmentMainPage.mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    coordinates[0] = location.getLatitude();
                    coordinates[1] = location.getLongitude();
                }
            });
        }
    }

    //從timeStamp取得時間
    public String getTime(Date date){
        String result = splitString(splitString(date.toString() , " ")[3] , ":")[0]+":"+splitString(splitString(date.toString() , " ")[3] , ":")[1];
        return result;
    }

    //分割字串
    public String []splitString(String s , String regex){
        String[] split = s.split(regex);
        return split;
    }
}
package com.purcharoo.purcharoo;

public class User {

    private String name , account ,  school , gender , selfiePath;
    private int wallet;
    private double rate;

    public User(String name, String account , String school , String gender, int wallet, double rate , String selfiePath) {
        this.name = name;
        this.account = account;
        this.school = school;
        this.gender = gender;
        this.wallet = wallet;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public String getAccount() {
        return account;
    }

    public String getSchool() {
        return school;
    }

    public int getWallet() {
        return wallet;
    }

    public double getRate() {
        return rate;
    }

    public String getGender() {
        return gender;
    }

    public String getSelfiePath() {
        return selfiePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public void setSelfiePath(String selfiePath) {
        this.selfiePath = selfiePath;
    }
}

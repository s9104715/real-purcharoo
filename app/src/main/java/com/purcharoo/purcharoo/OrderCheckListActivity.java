package com.purcharoo.purcharoo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

public class OrderCheckListActivity extends AppCompatActivity {

    private RecyclerView.LayoutManager layoutManager;
    //清單
    private RecyclerView recyclerView;
    public static MerchandisesAdapter merchandisesAdapter;
    public static ArrayList<ItemMerchandises> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_order_check_list);

        recyclerView = findViewById(R.id.merchandises_list);

        setupTopToolBar();
        setupRecyclerview();
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.order_check_list_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //設置底部清單
    public void setupRecyclerview(){

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());//垂直排版
        merchandisesAdapter = new MerchandisesAdapter(list , false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(merchandisesAdapter);
    }

    public static void setupList(ArrayList<ItemMerchandises> merchandisesList){
        list = new ArrayList<>();
        for(int i = 0 ; i < merchandisesList.size() ; i ++){
            list.add(merchandisesList.get(i));
        }
    }
}

package com.purcharoo.purcharoo;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;


public class TaskActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabs;
    private TaskFragmentTasks taskFragmentTasks;
    private TaskFragmentMyCurrentTasks taskFragmentMyCurrentTasks;

    //此活動
    public static TaskActivity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_task);

        tabs = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.view_pager);
        taskFragmentTasks = new TaskFragmentTasks();
        taskFragmentMyCurrentTasks = new TaskFragmentMyCurrentTasks();
        instance = this;

        //預設Fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.view_pager, new TaskFragmentTasks()).commit();

        setupTopToolBar();
        setupTabsLayout();
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.task_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //tabLayout設置
    public void setupTabsLayout(){
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(taskFragmentTasks , "瀏覽任務");
        adapter.addFragment(taskFragmentMyCurrentTasks , "目前任務");
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);
    }

    @Override
    public void finish() {
        super.finish();
        instance = null;
    }
}

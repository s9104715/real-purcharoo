package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class MerchandiseAdapter extends RecyclerView.Adapter<MerchandiseAdapter.ExampleViewHolder>{

    private List<ItemMerchandise> list;//清單

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入merchandise_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        private TextView merchandiseName;//商品名
        private TextView unitPrice;//單價

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener) {
            super(itemView);

            merchandiseName = itemView.findViewById(R.id.merchandise_name);
            unitPrice = itemView.findViewById(R.id.unit_price);

            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public MerchandiseAdapter(List<ItemMerchandise> list) {
        this.list = list;
    }

    //Adapter載入merchandise_item.xml方法
    @NonNull
    @Override
    public MerchandiseAdapter.ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_merchandise,
                parent, false);
        MerchandiseAdapter.ExampleViewHolder viewHolder= new MerchandiseAdapter.ExampleViewHolder(v, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        ItemMerchandise currentItem = list.get(position);

        holder.merchandiseName.setText(currentItem.getMerchandiseName());
        holder.unitPrice.setText("$"+String.valueOf(currentItem.getUnitPrice()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

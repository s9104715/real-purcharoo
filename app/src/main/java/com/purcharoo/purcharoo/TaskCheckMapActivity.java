package com.purcharoo.purcharoo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class TaskCheckMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker marker;

    //firestore
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_task_check_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        setupTopToolBar();
    }

    public void setupTopToolBar() {

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.task_check_map_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (getApplicationContext() != null) {
            mMap.setInfoWindowAdapter(new InfoWindowAdapter(getApplicationContext()));
        }

        setupCurrentMarker();
        loadMarker();

    }

    public void setupCurrentMarker() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        FragmentMainPage.mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                LatLng coordinate = new LatLng(location.getLatitude() , location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions().position(coordinate).title("位置").snippet("您目前的位置");
                marker = mMap.addMarker(markerOptions);
            }
        });
    }

    //依據訂單資料在地圖上新增標籤
    public void loadMarker(){

        final ArrayList<String> schList = new ArrayList<>();
        final ArrayList<String> nameList = new ArrayList<>();
        //取得學校名稱
        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    schList.add(documentSnapshot.getId());//取得學校名稱
                }
                for( int i = 0 ; i <schList.size() ; i ++){
                    firestore.collection("school").document(schList.get(i)).collection("store")
                            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {

                                        for (int j = 0; j < TaskFragmentTasks.itemTask.getOrders().size(); j++) {
                                            if (documentSnapshot.getString("storeName").equals(TaskFragmentTasks.itemTask.getOrders().get(j).getStoreName())) {
                                                if (documentSnapshot.exists()) {

                                                    GeoPoint geo = documentSnapshot.getGeoPoint("position");
                                                    String storeName = documentSnapshot.getString("storeName");
                                                    LatLng taskMap = new LatLng(geo.getLatitude(),geo.getLongitude());

                                                    //取得商品名稱
                                                    for(int k = 0 ; k < TaskFragmentTasks.itemTask.getOrders().get(j).getMerchandiseList().size();k++){
                                                        nameList.add(TaskFragmentTasks.itemTask.getOrders().get(j).getMerchandiseList().get(k).getMerchandiseName());
                                                    }
                                                    //加入snippet
                                                    String snippet = "包含：";
                                                    for(int i = 0 ; i < nameList.size() ; i ++){
                                                        snippet = snippet+"\n"+nameList.get(i);//資料都要換行
                                                    }

                                                    MarkerOptions markerOptions = new MarkerOptions().position(taskMap).title(storeName).snippet(snippet);
                                                    mMap.addMarker(markerOptions);
                                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(taskMap,12));
                                                }

                                            }
                                        }

                                    }
                                }
                            });
                }
            }
        });
    }
}



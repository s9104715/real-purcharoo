package com.purcharoo.purcharoo;

public class RandomPicName {

    private String picName;

    public RandomPicName() {
    }

    public RandomPicName(String picName) {
        this.picName = picName;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String randomPicName) {
        this.picName = randomPicName;
    }
}

package com.purcharoo.purcharoo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import at.markushi.ui.CircleButton;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccSettingActivity extends AppCompatActivity implements View.OnClickListener  {

    //更改頭像按鈕元件
    private CircleButton changeSelfieBnt;
    private AlertDialog changeSelfieDialog;

    //會員資料元件
    private static CircleImageView selfie;
    private static EditText nameET;
    private static EditText genderET;
    private static EditText schET;
    private static EditText schMailET;
    private Uri selfieUri;//大頭貼uri
    private File selfieFile;//大頭貼檔案

    //firestore
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    private StorageReference storageRef;
    private StorageReference imageRef;
    private String downloadUri;

    //camera
    private static final int CAMERA_REQUEST = 1888;
    //gallery
    private static final int GALLERY_REQUEST = 1889;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_acc_setting);

        //更改按鈕設置
        changeSelfieBnt = (CircleButton)findViewById(R.id.change_selfie_bnt);
        changeSelfieBnt.setOnClickListener(this);

        //會員資料設置
        selfie = (CircleImageView) findViewById(R.id.selfie);
        nameET = (EditText)findViewById(R.id.name_et);
        genderET = (EditText)findViewById(R.id.gender_et);
        schET = (EditText)findViewById(R.id.school_et);
        schMailET = (EditText)findViewById(R.id.sch_mail_et);

        nameET.setFocusable(false);
        genderET.setFocusable(false);
        schET.setFocusable(false);
        schMailET.setFocusable(false);

        nameET.setOnClickListener(this);
        genderET.setOnClickListener(this);
        schET.setOnClickListener(this);
        schMailET.setOnClickListener(this);
        selfie.setOnClickListener(this);

        storageRef = FirebaseStorage.getInstance().getReference();//storage初始化

        //處理帳號資料
        setupMemberData();
        //topToolbar
        setupTopToolBar();
    }

    //處理帳號資料
    public void setupMemberData(){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        //處理帳號資料
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            nameET.setText(documentSnapshot.getString("name"));
                            genderET.setText(documentSnapshot.getString("gender"));
                            schET.setText(documentSnapshot.getString("school"));
                            schMailET.setText(auth.getCurrentUser().getEmail());
                            //firebase設置大頭貼
                            Picasso.get()
                                   .load(documentSnapshot.getString("selfiePath"))
                                    //圖片使用最低分辨率,降低使用空間大小
                                    .fit()
                                    .centerCrop()
                                   .into(selfie);
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_selfie_bnt:
                //更改照片方法對話框
                AlertDialog.Builder ADBuider = new AlertDialog.Builder(AccSettingActivity.this);
                ADBuider.setTitle("請選擇");
                View v = getLayoutInflater().inflate(R.layout.dialog_change_selfie,null);
                ADBuider.setView(v);
                changeSelfieDialog = ADBuider.create();
                changeSelfieDialog.show();
                break;
            case R.id.name_et:
                startActivity(new Intent(this , ChangeNameActivity.class));
                break;
            case R.id.gender_et:
                startActivity(new Intent(this , ChangeGenderActivity.class));
                break;
            case R.id.school_et:
                Toast.makeText(this , "學校會由系統依據信箱自動判別" , Toast.LENGTH_SHORT).show();
                break;
            case R.id.sch_mail_et:
                startActivity(new Intent(this , ChangeMailActivity.class));
                break;
            case R.id.selfie:
                startActivity(new Intent(this , CheckSelfieActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        updateSelfie();
        super.onBackPressed();
    }

    //修改學校資料(待確認學校信箱)
    public static void updateSchool(){

        String mail = auth.getCurrentUser().getEmail().toString().trim();
        String school = null;
        if(mail.contains("fju")){
            school = "輔仁大學";
        }else if(mail.contains("nccu")){
            school = "政治大學";
        }else if(mail.contains("ntu")){
            school = "台灣大學";
        }else if(mail.contains("tku")){
            school = "淡江大學";
        }else if(mail.contains("yzu")){
            school = "元智大學";
        }
        //修改學校
        Map<String,Object> changedata = new HashMap<>();
        changedata.put("school" , school);
        firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                .set(changedata , SetOptions.mergeFields("school"));
        schET.setText(school);
    }

    //對話框相機按鍵
    public void CameraOnClick(View v){
        changeSelfieDialog.dismiss();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        selfieFile = new File(getExternalCacheDir(),
                String.valueOf(System.currentTimeMillis()) + ".jpg");
        selfieUri = Uri.fromFile(selfieFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selfieUri);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    //對話框選擇相片按鍵
    public void GalleryOnClick(View v){
        changeSelfieDialog.dismiss();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST);
    }

    public void deleteOnClick(View v){
        final String defaultSelfiePath = "https://firebasestorage.googleapis.com/v0/b/purcharoo.appspot.com/o/memberSelfie%2Fdefault.png?alt=media&token=d22d7087-96ab-4004-93cc-0dcfa2349f18";
        //照片還原成default
        Map<String,Object> changedata = new HashMap<>();
        changedata.put("selfiePath" , defaultSelfiePath);
        firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                .set(changedata , SetOptions.mergeFields("selfiePath"));
        //刷新selfie
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            //firebase設置大頭貼
                            Picasso.get()
                                    .load(documentSnapshot.getString("selfiePath"))
                                    //圖片使用最低分辨率,降低使用空間大小
                                    .fit()
                                    .centerCrop()
                                    .into(selfie);
                                Toast.makeText(getApplicationContext() , "照片已經刪除" , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        changeSelfieDialog.dismiss();
    }

    //對話框返回鍵
    public void BackOnClick(View v){
        changeSelfieDialog.dismiss();
    }

    //讀取相片 & gallery回傳變數
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //相機
            if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){
                Bitmap cameraBitmap = null;
                try{
                    cameraBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),selfieUri);
                    selfie.setImageBitmap(cameraBitmap);//修改照片
                }catch (IOException e) {
                    e.printStackTrace();
                }
                if (selfieUri != null) {
                    //上傳照片
                    imageRef = storageRef.child("memberSelfie/" + auth.getCurrentUser().getUid() + ".png");
                    imageRef.putFile(selfieUri);
                }

                //選擇相片
            }else if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null){
                selfieUri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),selfieUri);
                    selfie.setImageBitmap(bitmap);
                }
                catch (IOException e){
                    e.printStackTrace();
                }
                if(selfieUri != null){
                    //上傳照片
                    imageRef = storageRef.child("memberSelfie/" + auth.getCurrentUser().getUid() + ".png");
                    imageRef.putFile(selfieUri);
                }
            }
    }
    //取得download uri，更新主頁、setting大頭貼
    public void updateSelfie(){
        storageRef.child("memberSelfie/"+auth.getCurrentUser().getUid()+".png").getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        downloadUri = uri.toString();
                        //修改大頭貼存取路徑
                        Map<String,Object> changedata = new HashMap<>();
                        changedata.put("selfiePath" , downloadUri);
                        firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                                .set(changedata , SetOptions.mergeFields("selfiePath"));
                        //更新主頁測選單資料
                        MainActivity.update(downloadUri);
                        //更新setting頁的資料
                        FragmentSetting.update(downloadUri);
                    }
                });
    }
    //更新會員資料
    public static void update(){
        //處理帳號資料
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            nameET.setText(documentSnapshot.getString("name"));
                            genderET.setText(documentSnapshot.getString("gender"));
                            schMailET.setText(auth.getCurrentUser().getEmail());
                        }
                    }
                });
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.acc_setting_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}

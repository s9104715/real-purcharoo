package com.purcharoo.purcharoo;

import java.util.ArrayList;

public class ItemOrder {

    private String storeName;
    private String detail;
    private ArrayList<ItemMerchandises> merchandiseList;//商品清單
    private int totalPrice;

    public ItemOrder() {
    }

    public ItemOrder(String storeName, String detail, ArrayList<ItemMerchandises> merchandiseList) {
        this.storeName = storeName;
        this.detail = detail;
        this.merchandiseList = merchandiseList;
        calTotalPrice();//處裡總價
    }

    public ItemOrder(String storeName, String detail, ArrayList<ItemMerchandises> merchandiseList, int totalPrice) {
        this.storeName = storeName;
        this.detail = detail;
        this.merchandiseList = merchandiseList;
        this.totalPrice = totalPrice;
    }

    public String getStoreName() {
        return storeName;
    }

    public String getDetail() {
        return detail;
    }

    public ArrayList<ItemMerchandises> getMerchandiseList() {
        return merchandiseList;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setMerchandiseList(ArrayList<ItemMerchandises> merchandiseList) {
        this.merchandiseList = merchandiseList;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    //總價
    public void calTotalPrice() {
        int sum = 0;
        for(int i = 0 ; i < merchandiseList.size() ; i ++){
            sum += merchandiseList.get(i).getTotalPrice();
        }
        setTotalPrice(sum);
    }
}

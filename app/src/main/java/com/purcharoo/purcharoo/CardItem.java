package com.purcharoo.purcharoo;

public class CardItem {

    private int storeImgRes;//卡片圖片資源id
    private String storeName;

    public CardItem(int storeImgRes, String storeName) {
        this.storeImgRes = storeImgRes;
        this.storeName = storeName;
    }

    public int getStoreImgRes() {
        return storeImgRes;
    }

    public String getStoreName() {
        return storeName;
    }
}
package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;

public class ChangeMailActivity  extends AppCompatActivity implements View.OnClickListener {

    private EditText mailChangeET;
    private String mailTemp;
    private EditText verCodeET;
    private Button sendCodeBnt;
    private Button comfirmBnt;
    private String verCode = null;

    //firestore
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_change_mail);

        mailChangeET = (EditText)findViewById(R.id.mail_change_et);
        verCodeET = (EditText)findViewById(R.id.verify_code_et);
        sendCodeBnt = (Button)findViewById(R.id.send_code_bnt);
        comfirmBnt = (Button)findViewById(R.id.comfirm_bnt);
        sendCodeBnt.setOnClickListener(this);
        comfirmBnt.setOnClickListener(this);

        setupMailET();

        //pop up method
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        getWindow().setLayout(1200, 1000);//window size

    }

    public void setupMailET(){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        //處理信箱資料
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            mailChangeET.setText(auth.getCurrentUser().getEmail().trim());
                            mailTemp = auth.getCurrentUser().getEmail().trim();//暫存
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.send_code_bnt:
                verCode = randomCode();
                sendEmail(verCode);
                break;
            case R.id.comfirm_bnt:
                if(mailChangeET.getText().toString().equals(mailTemp)){
                    super.onBackPressed();
                }else {
                    if(mailChangeET.getText().toString().trim().isEmpty()){
                        mailChangeET.setError("學校信箱不能為空");
                        mailChangeET.requestFocus();
                        return;
                    }
                    if(!Patterns.EMAIL_ADDRESS.matcher(mailChangeET.getText().toString().trim()).matches()){
                        mailChangeET.setError("信箱格式不正確");
                        mailChangeET.requestFocus();
                        return;
                    }
                    //目前僅有輔仁大學信箱
                    if(!mailChangeET.getText().toString().trim().contains("mail.fju.edu.tw")){
                        mailChangeET.setError("需要學校信箱");
                        mailChangeET.requestFocus();
                        return;
                    }
                    if(!verCodeET.getText().toString().equals(verCode)){
                        verCodeET.setError("驗證碼不正確");
                        verCodeET.requestFocus();
                        return;
                    }
                    auth.getCurrentUser().updateEmail(mailChangeET.getText().toString());
                    finish();
                    //更新accSetting介面
                    AccSettingActivity.updateSchool();//先更新firebase學校資料
                    AccSettingActivity.update();
                }
            break;
        }
    }

    public void sendEmail(String verCode){

        if(mailChangeET.getText().toString().trim().isEmpty()){
            mailChangeET.setError("學校信箱不能為空");
            mailChangeET.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(mailChangeET.getText().toString().trim()).matches()){
            mailChangeET.setError("信箱格式不正確");
            mailChangeET.requestFocus();
            return;
        }
        //目前僅有輔仁大學信箱
        if(!mailChangeET.getText().toString().trim().contains("mail.fju.edu.tw")){
            mailChangeET.setError("需要學校信箱");
            mailChangeET.requestFocus();
            return;
        }
        if(mailChangeET.getText().toString().equals(mailTemp)){
            mailChangeET.setError("信箱為舊信箱");
            mailChangeET.requestFocus();
            return;
        }
        MailSender sender = new MailSender(this , mailChangeET.getText().toString().trim() , "您的Purcharoo驗證碼" , "您的驗證碼為："+verCode);
        sender.execute();//send email

    }

    //產生驗證碼
    public String randomCode(){
        final String DATA= "0123456789";
        Random random = new Random();
        StringBuilder code = new StringBuilder(4);

        for(int i = 0 ; i < code.capacity() ; i++){
            code.append(DATA.charAt(random.nextInt(DATA.length())));
        }
        return code.toString();
    }
}

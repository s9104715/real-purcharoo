package com.purcharoo.purcharoo;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

class TabsAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> tabsList = new ArrayList<>();
    private final ArrayList<String> tabsTitleList = new ArrayList<>();

    public TabsAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment , String title){
        tabsList.add(fragment);
        tabsTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabsTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return tabsList.get(position);
    }

    @Override
    public int getCount() {
        return tabsList.size();
    }
}

package com.purcharoo.purcharoo;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class BrowseArticlePicActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private TextView currentPage;
    private TextView maxPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_check_article_pic);

        viewPager = findViewById(R.id.pic_view_pager);
        currentPage = findViewById(R.id.current_page);
        maxPage = findViewById(R.id.max_page);

        setupViewPager();
    }

    public void setupViewPager(){

        viewPagerAdapter = new ViewPagerAdapter(ArticleActivity.picUri , getApplicationContext() , false);
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i){
                    case 0:
                        currentPage.setText("1");
                        break;
                    case 1:
                        currentPage.setText("2");
                        break;
                    case 2:
                        currentPage.setText("3");
                        break;
                    case 3:
                        currentPage.setText("4");
                        break;
                    case 4:
                        currentPage.setText("5");
                        break;
                    case 5:
                        currentPage.setText("6");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        //總頁數
        maxPage.setText(ArticleActivity.picUri.size()+"");
    }
}

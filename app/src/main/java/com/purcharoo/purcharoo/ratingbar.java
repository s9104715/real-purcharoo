package com.purcharoo.purcharoo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

public class ratingbar extends AppCompatActivity {
    RatingBar ratingBar;
    Button button;
    EditText editText;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ratingbar);

        editText = (EditText) findViewById(R.id.editText);
        ratingBar=(RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setNumStars(5);
        button=(Button) findViewById(R.id.button);


        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Toast.makeText(ratingbar.this,"star"+ (int)v,Toast.LENGTH_SHORT).show();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ratingbar.this, "Star:"+(int)ratingBar.getRating(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}

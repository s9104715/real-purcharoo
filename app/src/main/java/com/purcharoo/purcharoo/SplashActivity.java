package com.purcharoo.purcharoo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashActivity extends AppCompatActivity {

    private ImageView logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo = findViewById(R.id.logo);

        Animation myanim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in);
        logo.startAnimation(myanim);

        final Intent i = new Intent(SplashActivity.this, MainActivity.class);
        Thread timer = new Thread() {
            public void run() {
                try {

                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(i);
                    finish();
                }
            }

        };
        timer.start();

    }
}

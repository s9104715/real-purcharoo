package com.purcharoo.purcharoo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

import at.markushi.ui.CircleButton;

public class TimerActivity extends AppCompatActivity {

    private TimePicker timer;
    private CircleButton confrimBnt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_timer);

        timer = findViewById(R.id.timer);
        confrimBnt = findViewById(R.id.confirm_bnt);

        setWindowSize();
        setTime();
    }

    //設定時間傳到OrderFragmentOrders的timeET
    public void setTime(){

        confrimBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //設定的時間至少比現在時間晚二小時(接單者跑單至少需二小時)
                if(calHourDiffrence(timer.getCurrentHour() , timer.getCurrentMinute() , Calendar.getInstance().getTime().getHours() , Calendar.getInstance().getTime().getMinutes()) >= 2 || calHourDiffrence(timer.getCurrentHour() , timer.getCurrentMinute() , Calendar.getInstance().getTime().getHours() , Calendar.getInstance().getTime().getMinutes()) < 0){
                    if(timer.getCurrentMinute() < 10){//數字小於10 要補0
                        OrderFragmentOrders.timeET.setText(timer.getCurrentHour() +": 0"+timer.getCurrentMinute());
                    }else {
                        OrderFragmentOrders.timeET.setText(timer.getCurrentHour() +":"+timer.getCurrentMinute());
                    }
                    finish();
                    OrderFragmentOrders.hasSelectTime = true;//已有選時間
                }else {
                    Toast.makeText(getApplicationContext() , "取餐時間必須設定在至少兩小時之後" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //計算小時差
    public double calHourDiffrence(int timerHour , int timerMin , int currentHour , int currentMin){

        int hourDiff = timerHour - currentHour;
        double minDiff = (double) (timerMin - currentMin) / 60;

        return hourDiff + minDiff;
    }

    public void setWindowSize(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        getWindow().setLayout(width - 200, height -800);//window size
    }
}

package com.purcharoo.purcharoo;

import android.app.Service;
import android.content.Context;
import android.net.Uri;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import at.markushi.ui.CircleButton;

public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<String> imagesUri;
    private LayoutInflater layoutInflater;
    private Context ctx;
    private boolean onClickMethod;

    public ViewPagerAdapter(ArrayList<String> imagesUri , Context ctx , boolean onClickMethod) {
        this.imagesUri = imagesUri;
        this.ctx = ctx;
        this.onClickMethod = onClickMethod;
    }

    @Override
    public int getCount() {
        return imagesUri.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //use uri
        View view;
        ImageView imageView;
        layoutInflater = LayoutInflater.from(ctx);
        view = layoutInflater.inflate(R.layout.item_view_pager_image, container , false);
        imageView = (ImageView) view.findViewById(R.id.view_pager_image);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        Picasso.get().load(imagesUri.get(position))
                //圖片使用最低分辨率,降低使用空間大小
                .fit()
                .centerCrop()
                .into(imageView);//取得大頭貼

        if(onClickMethod){
                imageView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        CheckArticlePicActivity.showDeleteBnt();//show bnt
                        return true;
                    }
                });
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CheckArticlePicActivity.hideDeleteBnt();//hide bnt
                    }
                });
            }

        container.addView(view , 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout)object);
    }
}

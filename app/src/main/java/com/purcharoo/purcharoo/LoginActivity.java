package com.purcharoo.purcharoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText accET , passET;
    private Button logBnt;
    private TextView reg;
    //login
    private FirebaseAuth auth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_login);

        auth = FirebaseAuth.getInstance();

        reg = findViewById(R.id.reg);
        reg.setOnClickListener(this);

        logBnt = (Button) findViewById(R.id.log_bnt);
        logBnt.setOnClickListener(this);

        accET = (EditText) findViewById(R.id.num1);
        passET = (EditText) findViewById(R.id.num2);

    }

    //登入方法
    public void loginUser(){

        String account = accET.getText().toString().trim();
        String password = passET.getText().toString().trim();

        if(account.isEmpty()){
            accET.setError("請輸入帳號");
            accET.requestFocus();
            return;
        }
        if(password.isEmpty()){
            passET.setError("請輸入密碼");
            passET.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(account).matches()){
            accET.setError("電子郵件不正確");
            accET.requestFocus();
            return;
        }
        if(password.length() < 8){
            passET.setError("密碼至少需8位數");
            passET.requestFocus();
            return;
        }
        //啓動procgress dialog
        showProcessDialog();

        auth.signInWithEmailAndPassword(account , password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressDialog.dismiss();
                if(task.isSuccessful()){
                    finish();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//銷毀目前的activity和其上的activity,並重新創建新的activity
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this,"錯誤的帳號或密碼",Toast.LENGTH_SHORT ).show();
                }
            }
        });
    }

    //自定progress dialog
    private void showProcessDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("登入");
        progressDialog.setMessage("登入中.....");
        progressDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.log_bnt:
                loginUser();
                break;
            case R.id.reg:
                finish();
                //registActivity
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //如果已登入直接進入主頁
        if(auth.getCurrentUser()!= null){
            finish();
            startActivity(new Intent(this , MainActivity.class));
        }
    }
}

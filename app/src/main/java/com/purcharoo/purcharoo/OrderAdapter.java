package com.purcharoo.purcharoo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.ArrayList;
import java.util.List;
import at.markushi.ui.CircleButton;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ExampleViewHolder> {

    private List<ItemOrder> list;//清單
    private ExampleViewHolder viewHolder;
    private boolean onLongClickPermit;//是否允許長案事件

    //firebase
    private FirebaseFirestore firestore;
    private ArrayList<String> schoolList;

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入order_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        private TextView storeName;//店名
        private TextView detail;//細目
        private TextView totalPrice;//總價
        private CircleButton deleteItemBnt;//刪除訂單按鈕
        private boolean itemLongClick;//刪除按鈕是否出現

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener , boolean onLongClickPermit) {
            super(itemView);

            storeName = itemView.findViewById(R.id.store_name);
            detail = itemView.findViewById(R.id.detail);
            totalPrice = itemView.findViewById(R.id.total_price);
            deleteItemBnt = itemView.findViewById(R.id.delete_bnt);

            //監聽器設置
                    itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (listener != null) {
                                int position = getAdapterPosition();
                                if (position != RecyclerView.NO_POSITION) {
                                    listener.onItemClick(position);
                                }
                            }
                            deleteItemBnt.setVisibility(View.INVISIBLE);
                            itemLongClick = false;
                }
            });

            //長按事件　deleteBnt出現
            if(onLongClickPermit){
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        deleteItemBnt.setVisibility(View.VISIBLE);
                        itemLongClick = true;
                        return true;
                    }
                });
            }
        }

        public boolean isItemLongClick() {
            return itemLongClick;
        }
    }

    public OrderAdapter(List<ItemOrder> list , boolean onLongClickPermit) {
        this.list = list;
        this.onLongClickPermit = onLongClickPermit;
        schoolList = new ArrayList<>();
    }

    //Adapter載入order_item.xml方法
    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order,
                parent, false);
        viewHolder= new ExampleViewHolder(v, clickListener , onLongClickPermit);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ExampleViewHolder holder, final int position) {

        final ItemOrder currentItem = list.get(position);

        //載入學校名稱
        holder.storeName.setText(currentItem.getStoreName());
        holder.detail.setText(currentItem.getDetail());
        holder.totalPrice.setText("$"+currentItem.getTotalPrice());

        holder.deleteItemBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteOrder(position);
                holder.deleteItemBnt.setVisibility(View.INVISIBLE);

                //處裡總和
                OrderFragmentOrders.setupTotal(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //新增item
    public void addOrder(ItemOrder itemOrder){
        list.add(itemOrder);
        notifyDataSetChanged();
    }

    //刪除特定item
    public void deleteOrder(int position){
        list.remove(position);
        notifyDataSetChanged();
    }

    public List<ItemOrder> getList() {
        return list;
    }

    public ExampleViewHolder getViewHolder() {
        return viewHolder;
    }
}

package com.purcharoo.purcharoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import at.markushi.ui.CircleButton;

public class CheckArticlePicActivity extends AppCompatActivity implements View.OnClickListener {

    private static ViewPager picViewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private TextView currentPage;
    private TextView maxPage;
    private ConstraintLayout constraintLayout;
    public static CircleButton deleteBnt;//要給viewPagerAdapter取得

    //firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;
    private String deletePicUri;
    private String deletePicName;
    private String deleteDocumentId;
    //processDialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_check_article_pic);

        picViewPager = (ViewPager)findViewById(R.id.pic_view_pager);
        currentPage = (TextView)findViewById(R.id.current_page);
        maxPage = (TextView)findViewById(R.id.max_page);
        constraintLayout = (ConstraintLayout)findViewById(R.id.check_article_pic_layout);
        deleteBnt = (CircleButton)findViewById(R.id.delete_bnt);

        constraintLayout.setOnClickListener(this);
        deleteBnt.setOnClickListener(this);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();

        setupViewPager();
    }

    public void setupViewPager() {
        //setup viewPager
        //清洗多餘資料
        washPicData(EditArticleActivity.picDocumentId);
        washPicData(EditArticleActivity.picNameArray);
        washPicData(EditArticleActivity.picUriArray);
        viewPagerAdapter = new ViewPagerAdapter(EditArticleActivity.picUriArray , getApplicationContext() , true );
        picViewPager.setAdapter(viewPagerAdapter);
        //目前頁數
        picViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i){
                    case 0:
                        currentPage.setText("1");
                        break;
                    case 1:
                        currentPage.setText("2");
                        break;
                    case 2:
                        currentPage.setText("3");
                        break;
                    case 3:
                        currentPage.setText("4");
                        break;
                    case 4:
                        currentPage.setText("5");
                        break;
                    case 5:
                        currentPage.setText("6");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        //總頁數
        maxPage.setText(EditArticleActivity.picUriArray.size()+"");
    }

    public static void showDeleteBnt(){

       deleteBnt.setVisibility(View.VISIBLE);
    }

    public static void hideDeleteBnt(){

        deleteBnt.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.delete_bnt:
                for(int i = 0 ; i < EditArticleActivity.picUriArray.size() ; i ++) {
                    if (picViewPager.getCurrentItem() == i) {

                        deletePicUri = EditArticleActivity.picUriArray.get(i);//欲刪除的照片uri
                        deletePicName = getPicNameFromUri(deletePicUri);//欲刪除的照片名稱
                        EditArticleActivity.picUriArray.remove(i);
                    }
                }
                        //remove DB
                        //尋找每筆文件
                for(int j = 0 ; j < EditArticleActivity.picDocumentId.size() ; j ++){
                    //讀取該刪除的文件
                    firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).collection("picName")
                            .document(EditArticleActivity.picDocumentId.get(j)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if(documentSnapshot.exists()){
                                        if(documentSnapshot.getString("picName").equals(deletePicName)){
                                            deleteDocumentId = documentSnapshot.getId();
                                            //刪除picName文件
                                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).collection("picName")
                                                    .document(deleteDocumentId).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }

                        //刪除照片
                        storageRef.child("articlePic/"+EditArticleActivity.picFileName+"/" +deletePicName+ ".png").delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                    }
                                });

                        //修改照片數量
                        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        //照片數量-1
                                        Map<String,Object> changedata = new HashMap<>();
                                        changedata.put("picNum" , documentSnapshot.get("picNum" , Integer.TYPE) - 1);
                                        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                                .set(changedata , SetOptions.mergeFields("picNum"));
                                    }
                                });

                //陣列刪除元素
                for(int i = 0 ; i < EditArticleActivity.picDocumentId.size() ; i ++){
                    if(EditArticleActivity.picDocumentId.get(i).equals(deleteDocumentId)){
                        EditArticleActivity.picDocumentId.remove(i);
                    }
                }
                for(int j = 0 ; j < EditArticleActivity.picNameArray.size() ; j ++){
                    if(EditArticleActivity.picNameArray.get(j).equals(deletePicName)){
                        EditArticleActivity.picNameArray.remove(j);
                    }
                }
                deletePicDialog();
                setProgressTime(progressDialog , 2000);//2秒
                break;
            case R.id.check_article_pic_layout:
                //點擊任何地方隱藏刪除按鈕
                deleteBnt.setVisibility(View.INVISIBLE);
                break;
        }
    }

    public String getPicNameFromUri(String uri){
        String[] token1 = uri.split("%2F");
        String[] token2 = token1[2].split(".png");
        return token2[0];
    }

    //progress dialog 為了能讓資料庫有時間上傳好圖片
    private void deletePicDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("刪除圖片");
        progressDialog.setMessage("刪除中.....");
        progressDialog.show();
    }
    //progress dialog設定停滯時間
    public void setProgressTime(final ProgressDialog progressDialog , int delayMillis){
        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                //刷新
                onBackPressed();
                if(EditArticleActivity.picUriArray.size() > 0){
                    startActivity(new Intent(getApplicationContext() , CheckArticlePicActivity.class));
                }
                Toast.makeText(getApplicationContext() , "照片已刪除" , Toast.LENGTH_SHORT).show();
            }
        };
        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, delayMillis);//毫秒計算
    }

    //把多餘的資料洗掉
    public void washPicData(final ArrayList<String> arrayList) {

        LinkedHashSet<String> set = new LinkedHashSet<>(arrayList);
        ArrayList<String> cleanArrayList = new ArrayList<>(set);//乾淨的arraylist
        //複製
        arrayList.clear();
        arrayList.addAll(cleanArrayList);
    }
}

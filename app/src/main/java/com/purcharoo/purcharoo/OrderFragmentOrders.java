package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;
import java.util.List;

import at.markushi.ui.CircleButton;

public class OrderFragmentOrders extends Fragment implements View.OnClickListener {

    public static List<ItemOrder> orders;//訂單清單
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    public static OrderAdapter orderAdapter;
    private CircleButton addItemBnt;//新增訂單按鈕
    private Button confirmOrderBnt;
    //總計
    public static TextView totalTV;
    public static TextView total;
    //時間
    public static EditText timeET;
    public static boolean hasSelectTime = false;//是否有選擇過時間
    //備註
    public static EditText remarkET;
    //返回AlertDialog
    private AlertDialog backAlertDialog;
    //Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_order_orders, container, false);

        recyclerView = view.findViewById(R.id.merchandise_list);
        addItemBnt = view.findViewById(R.id.add_bnt);
        totalTV = view.findViewById(R.id.total_tv);
        total = view.findViewById(R.id.total);
        timeET = view.findViewById(R.id.time_ET);
        remarkET = view.findViewById(R.id.remark);
        confirmOrderBnt = view.findViewById(R.id.confirm_order_bnt);

        //元件設置
        totalTV.setVisibility(View.INVISIBLE);
        total.setVisibility(View.INVISIBLE);
        timeET.setFocusable(false);
        timeET.setOnClickListener(this);
        addItemBnt.setOnClickListener(this);
        confirmOrderBnt.setOnClickListener(this);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        orders = new ArrayList<>();

        //setup order
        setupRecyclerView();

        return view;
    }

    private void setupRecyclerView() {

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        orderAdapter = new OrderAdapter(orders , true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(orderAdapter);

        //查看清單
        orderAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(!orderAdapter.getViewHolder().isItemLongClick()){//刪除鍵已出現
                    OrderCheckListActivity.setupList(orders.get(position).getMerchandiseList());
                    startActivity(new Intent(getContext() , OrderCheckListActivity.class));
                }
            }
        });
    }

    public static void addOrder(ItemOrder itemOrder){
        orderAdapter.addOrder(itemOrder);
    }

    //計算總和
    public static void setupTotal(boolean show){

        int sum = 0;
        if(show){//有order時
            //總計顯示
            totalTV.setVisibility(View.VISIBLE);
            total.setVisibility(View.VISIBLE);
            //計算
            for(int i = 0 ; i < orderAdapter.getList().size() ; i ++){
                sum += orderAdapter.getList().get(i).getTotalPrice();
            }
            total.setText("$ "+String.valueOf(sum));
        }else {
            if(orderAdapter.getList().size() == 0) {
                //無清單時隱藏
                totalTV.setVisibility(View.INVISIBLE);
                total.setVisibility(View.INVISIBLE);
                total.setText("");
            }else {
                //計算
                for(int i = 0 ; i < orderAdapter.getList().size() ; i ++){
                    sum += orderAdapter.getList().get(i).getTotalPrice();
                }
                total.setText("$ "+String.valueOf(sum));
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_bnt:
                startActivity(new Intent(getContext(), OrderAddListActivity.class));
                break;
            case R.id.confirm_order_bnt:
                //進入地圖選擇地點
                if(!hasSelectTime || orders.size() == 0){//沒選商品或者時間時
                    Toast.makeText(getContext() , "必須選擇商品和時間！" , Toast.LENGTH_SHORT).show();
                }else {
                    //是否已創建任務
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myCurrentTask")
                            .document("task").get()
                            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                @Override
                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                    if(!documentSnapshot.exists()){
                                        startActivity(new Intent(getContext(), OrderMapActivity.class));
                                    }else {
                                        Toast.makeText(getContext() , "您已經創建了一個任務！" , Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
                break;
            case R.id.time_ET:
                startActivity(new Intent(getContext() , TimerActivity.class));
                break;
        }
    }
}

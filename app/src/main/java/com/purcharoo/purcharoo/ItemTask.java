package com.purcharoo.purcharoo;

import android.location.Location;

import java.util.List;

public class ItemTask {

    private String taskID;
    private String userUID;
    private String address;//目的地
    private double latitude;
    private double longitude;
    //自己所在位置
    private double [] coordinates;//兩筆資料,前為latitude 後為longitude
    private Double distance;
    private int totalPrice;//訂單者的付款
    private String time;//時限
    private List<ItemOrder> orders;//order清單
    private String remark;//備註
    private boolean assigned;//是否有人接單

    public ItemTask() {
    }

    public ItemTask(String taskID, String userUID, String address, double latitude, double longitude, double [] coordinates, int totalPrice, String time, List<ItemOrder> orders, String remark , boolean assigned) {
        this.taskID = taskID;
        this.userUID = userUID;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.coordinates = coordinates;
        this.totalPrice = totalPrice;
        this.time = time;
        this.orders = orders;
        this.remark = remark;
        this.assigned = assigned;
        setDistance(calDistance(coordinates[0] , coordinates[1] , latitude , longitude));
    }

    public String getTaskID() {
        return taskID;
    }

    public String getUserUID() {
        return userUID;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public String getTime() {
        return time;
    }

    public List<ItemOrder> getOrders() {
        return orders;
    }

    public String getRemark() {
        return remark;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Double getDistance() {
        return distance;
    }

    //取得距離
    public double calDistance(double startLatitude , double startLongitude , double endLatitude , double endLongitude){
        float [] distance = new float[10];
        Location.distanceBetween(startLatitude , startLongitude , endLatitude , endLongitude , distance);
        return distance[0]/1000;//公尺變公里
    }
}

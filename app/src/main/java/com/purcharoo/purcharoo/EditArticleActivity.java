package com.purcharoo.purcharoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.model.value.FieldValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.firestore.v1.DocumentTransform;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import at.markushi.ui.CircleButton;

public class EditArticleActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText titleET;
    private EditText articleET;
    private Spinner schoolSpinner;
    private CircleButton importPicBnt;
    private CircleButton confirmBnt;
    //返回AlertDialog
    private AlertDialog articleTempAlertDialog;
    //圖片AlertDialog
    private AlertDialog picAlertDialog;
    //文章發布確認AlertDialog
    private AlertDialog confirmAlertDialog;
    //processDialog
    private ProgressDialog progressDialog;
    private int progressCode; // 1 = 上傳圖片 2 = 發布文章

    //Firebase
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    private static StorageReference storageRef;
    private StorageReference imageRef;
    private Uri picUri;//照片uri
    public static String picFileName;//同時也是文章碼
    private String page;//校園版
    public static ArrayList<String> picNameArray;//圖片名稱
    public static ArrayList<String> picUriArray;//圖片uri陣列　交給CheckArticlePicActivity
    public static ArrayList<String> picDocumentId;//文件名稱
    private boolean createArticleIsSuccess;

    //gallery
    private final int GALLERY_REQUEST = 1889;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_edit_article);

        titleET = (EditText) findViewById(R.id.artical_title);
        articleET = (EditText) findViewById(R.id.artical_content);
        schoolSpinner = (Spinner) findViewById(R.id.school_spinner);
        importPicBnt = (CircleButton) findViewById(R.id.import_pic_bnt);
        confirmBnt = (CircleButton) findViewById(R.id.confirm_bnt);


        importPicBnt.setOnClickListener(this);//上傳照片bnt
        confirmBnt.setOnClickListener(this);//確認bnt

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        //storage初始化
        storageRef = FirebaseStorage.getInstance().getReference();
        //arrayList初始化
        picNameArray = new ArrayList<>();
        picUriArray = new ArrayList<>();
        picDocumentId = new ArrayList<>();

        setupSpinner();
        createArticleTemp();//default文章暫存
        setupTopToolBar();
        setupArticleTemp();//讀取儲存文章
        loadPicData();
    }

    public void setupSpinner(){

        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

            }
        });
        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                List<String> schList = new ArrayList<>();
                for(DocumentSnapshot snapshot : queryDocumentSnapshots){
                    schList.add(snapshot.getString("page"));
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext() , R.layout.spinner_item ,schList);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                schoolSpinner.setAdapter(spinnerAdapter);//載入學校清單
            }
        });
    }

    //取得時間
    public java.sql.Timestamp getTimeStamp(){
        Date d = new Date();
        java.sql.Timestamp timestamp = new java.sql.Timestamp(d.getTime());
        return timestamp;
    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.edit_article_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //文章暫存default
    public void createArticleTemp(){

        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        //檔案不存在
                        if(!documentSnapshot.exists()){
                            //初始化文章暫存
                            Article article = new Article("" , "" , "" , randomCode(8) , 0);
                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                    .set(article);
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {

        //有內容就跳出儲存文章的dialog
        if(!titleET.getText().toString().equals("") || !articleET.getText().toString().equals("")){

            final AlertDialog.Builder ADBuider = new AlertDialog.Builder(EditArticleActivity.this);
            ADBuider.setTitle("儲存文章");
            ADBuider.setMessage("是否要儲存未完成的文章");

            DialogInterface.OnClickListener articleTempADClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i){
                        case DialogInterface.BUTTON_POSITIVE:

                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                                            //修改articleTemp資料
                                            Map<String,Object> changedata = new HashMap<>();
                                            changedata.put("title" , titleET.getText().toString());
                                            changedata.put("page" , schoolSpinner.getSelectedItem().toString());
                                            changedata.put("content" , articleET.getText().toString());
                                            changedata.put("picFileName" , documentSnapshot.getString("picFileName"));//picFileName永遠不變
                                            changedata.put("picNum" , documentSnapshot.get("picNum" , Integer.TYPE));//交由onActivityResult更新圖片數量
                                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                                    .set(changedata);
                                        }
                                    });
                            FragmentShare.picUploaded = false;
                            EditArticleActivity.super.onBackPressed();
                            Toast.makeText(getApplicationContext() , "文章已儲存" , Toast.LENGTH_SHORT).show();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            FragmentShare.picUploaded = false;
                            EditArticleActivity.super.onBackPressed();
                            break;
                    }
                }
            };

            ADBuider.setPositiveButton("是" , articleTempADClickListener);
            ADBuider.setNegativeButton("否" , articleTempADClickListener);

            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(!titleET.getText().toString().equals(documentSnapshot.getString("title")) || !articleET.getText().toString().equals(documentSnapshot.getString("content")) || !schoolSpinner.getSelectedItem().toString().equals(documentSnapshot.getString("page"))){
                                //內容有修改
                                articleTempAlertDialog = ADBuider.create();
                                articleTempAlertDialog.show();
                            }else{
                                //內容沒修改時直接返回
                                FragmentShare.picUploaded = false;
                                EditArticleActivity.super.onBackPressed();
                            }
                        }
                    });
        }else {
            super.onBackPressed();
        }
    }

    //讀取儲存的文章
    public void setupArticleTemp(){

        if(!FragmentShare.picUploaded){
            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                //讀取成功，已有儲存的文章
                                //載入各資料
                                titleET.setText(documentSnapshot.getString("title"));
                                for (int i = 0; i < schoolSpinner.getAdapter().getCount(); i++) {
                                    if (schoolSpinner.getItemAtPosition(i).toString().equals(documentSnapshot.getString("page"))) {//讀取校園版
                                        schoolSpinner.setSelection(i); }
                                }
                                articleET.setText(documentSnapshot.getString("content"));
                                page = documentSnapshot.getString("page");
                            }
                        }
                    });
        }else {
            // load temp with pic
            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                    .collection("articleTempWithPic").document("temp").get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if(documentSnapshot.exists()){
                                //讀取articleTempWithPic
                                titleET.setText(documentSnapshot.getString("title"));
                                for (int i = 0; i < schoolSpinner.getAdapter().getCount(); i++) {
                                    if (schoolSpinner.getItemAtPosition(i).toString().equals(documentSnapshot.getString("page"))) {//讀取校園版
                                        schoolSpinner.setSelection(i); }
                                }
                                articleET.setText(documentSnapshot.getString("content"));
                                page = documentSnapshot.getString("page");
                            }
                        }
                    });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.import_pic_bnt:
                AlertDialog.Builder ADBuider = new AlertDialog.Builder(EditArticleActivity.this);
                ADBuider.setTitle("請選擇");
                View v = getLayoutInflater().inflate(R.layout.dialog_import_pic,null);
                ADBuider.setView(v);
                picAlertDialog = ADBuider.create();
                picAlertDialog.show();
                break;
            case R.id.confirm_bnt:
                if(articleET.getText().toString().isEmpty() || titleET.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext() , "標題和文章不能為空！" , Toast.LENGTH_SHORT).show();
                }else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditArticleActivity.this);
                    builder.setTitle("文章發布確認");
                    builder.setMessage("請問確定要在"+page+"發布文章嗎?");

                    DialogInterface.OnClickListener confirmADClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            switch (i){
                                case DialogInterface.BUTTON_POSITIVE:
                                    //發佈文章
                                    createArticle();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    //返回
                                    confirmAlertDialog.dismiss();
                                    break;
                            }
                        }
                    };
                    builder.setPositiveButton("是" , confirmADClickListener);
                    builder.setNegativeButton("否" , confirmADClickListener);
                    confirmAlertDialog = builder.create();
                    confirmAlertDialog.show();
                }
                break;
        }
    }
    //插入圖片
    public void ImportOnClick(View v){
        picAlertDialog.dismiss();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //處理文章暫存articleTempWithPic
        createArticleTempWithPic();
        modifyArticleTempWithPic();
        //gallery
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST);
    }
    //檢視圖片
    public void CheckPicOnClick(View v){
        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            //如果有圖片
                            if(documentSnapshot.get("picNum" , Integer.TYPE) > 0){
                                picAlertDialog.dismiss();
                                startActivity(new Intent(getApplicationContext() , CheckArticlePicActivity.class));//檢視圖片
                            }else {
                                Toast.makeText(getApplicationContext() , "未有圖片", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    //返回鍵
    public void BackPicOnClick(View v){
       picAlertDialog.dismiss();
    }

    //回傳Gallery傳回的變數
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null){
            picUri = data.getData();
            //上傳照片
            if(picUri != null){
                firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                if(documentSnapshot.exists()){
                                    //已有文章暫存
                                    if(documentSnapshot.get("picNum" , Integer.TYPE) < 6 ){

                                        String randomPicName = randomCode(8);//隨機圖片名稱
                                        //上傳圖片
                                        imageRef = storageRef.child("articlePic/" +documentSnapshot.getString("picFileName")+ "/"+randomPicName+".png");//文章路徑
                                        UploadTask uploadTask = imageRef.putFile(picUri);//上傳任務

                                        //新增文章圖片的名稱到articleTemp中的picName集合
                                        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                                .collection("picName").document(randomCode(6)).set(new RandomPicName(randomPicName));//文件、圖片名稱蕤機

                                        //更新圖片數量+1
                                        Map<String,Object> changedata = new HashMap<>();
                                        changedata.put("picNum" , documentSnapshot.get("picNum" , Integer.TYPE) + 1);
                                        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                                .set(changedata , SetOptions.mergeFields("picNum"));

                                        //代表已有圖片上傳成功
                                        FragmentShare.picUploaded = true;//使用articleTempWithPic

                                        //刷新EditActivity
                                        picDocumentId.clear();
                                        picNameArray.clear();
                                        picUriArray.clear();
                                        uploadPicDialog();//等待圖片上傳完畢
                                        //上傳完畢
                                        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                setProgressTime(progressDialog , 2000);//2秒後刷新
                                            }
                                        });
                                    }else{
                                        Toast.makeText(getApplicationContext() , "圖片已超過6張" , Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        });
            }
        }
    }

    //產生隨機名稱(8碼含英文大小寫)
    public String randomCode(int capacity){
        final String DATA= "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder code = new StringBuilder(capacity);

        for(int i = 0 ; i < code.capacity() ; i++){
            code.append(DATA.charAt(random.nextInt(DATA.length())));
        }
        return code.toString();
    }

    //存取picName每筆文件名稱
    public static void loadPicData(){

        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            picFileName = documentSnapshot.getString("picFileName");
                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                    .collection("picName").addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                            for (DocumentSnapshot snapshot : queryDocumentSnapshots) {
                                                picDocumentId.add(snapshot.getId());//取得picName集合中每筆文件名稱(6碼亂數)
                                                picNameArray.add(snapshot.getString("picName"));
                                                storageRef.child("articlePic/"+picFileName+"/" +snapshot.getString("picName")+ ".png").getDownloadUrl()
                                                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                            @Override
                                                            public void onSuccess(Uri uri) {
                                                                picUriArray.add(uri.toString());
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    //progress dialog 為了能讓資料庫有時間上傳好圖片
    private void uploadPicDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("上傳圖片");
        progressDialog.setMessage("上傳中.....");
        progressDialog.show();
        progressCode = 1;
    }

    //progress dialog 為了能讓資料庫有時間上傳好圖片
    private void createArticlePicDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("發布文章");
        progressDialog.setMessage("發布中.....");
        progressDialog.show();
        progressCode = 2;
    }

    //progress dialog設定停滯時間
    public void setProgressTime(final ProgressDialog progressDialog , int delayMillis){
        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                if(progressCode == 1){
                    //上傳圖片
                    progressDialog.dismiss();
                    EditArticleActivity.super.onBackPressed();
                    startActivity(new Intent(getApplicationContext() , EditArticleActivity.class));
                    Toast.makeText(getApplicationContext() , "圖片上傳成功" , Toast.LENGTH_SHORT).show();
                }else if(progressCode == 2){
                    if(createArticleIsSuccess){
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext() , "文章發布成功" , Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext() , "發布文章失敗，請再試一次。" , Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, delayMillis);//毫秒計算
    }

    //處理照片時新增的暫存
    public void createArticleTempWithPic(){
        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                .collection("articleTempWithPic").document("temp").get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(!documentSnapshot.exists()){
                            Article article = new Article("" , "" , "");
                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                    .collection("articleTempWithPic").document("temp")
                                    .set(article);
                        }
                    }
                });
    }
    //處理照片時修改暫存
    public void modifyArticleTempWithPic(){

        Map<String,Object> changedata = new HashMap<>();
        changedata.put("title" , titleET.getText().toString());
        changedata.put("page" , schoolSpinner.getSelectedItem().toString());
        changedata.put("content" , articleET.getText().toString());
        firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                .collection("articleTempWithPic").document("temp")
                .set(changedata);
    }

    //把多餘的資料洗掉
    public void washPicData(final ArrayList<String> arrayList) {

        LinkedHashSet<String> set = new LinkedHashSet<>(arrayList);
        ArrayList<String> cleanArrayList = new ArrayList<>(set);//乾淨的arraylist
        //複製
        arrayList.clear();
        arrayList.addAll(cleanArrayList);
    }

    //發布文章
    public void createArticle(){

        firestore.collection("article").document(picFileName).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(!documentSnapshot.exists()){
                            //輸出文章資料
                            confirmAlertDialog.dismiss();
                            createArticleIsSuccess = true;

                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(final DocumentSnapshot documentSnapshot) {
                                            //文章各個data
                                            Map<String,Object> changeData = new HashMap<>();
                                            changeData.put("title" , titleET.getText().toString());
                                            changeData.put("page" , schoolSpinner.getSelectedItem().toString());
                                            changeData.put("content" , articleET.getText().toString());
                                            changeData.put("picFileName" , picFileName);
                                            changeData.put("picNum" , documentSnapshot.get("picNum" , Integer.TYPE));
                                            changeData.put("time" , getTimeStamp());
                                            changeData.put("userAccount" , auth.getCurrentUser().getEmail());
                                            changeData.put("userUID" , auth.getCurrentUser().getUid());
                                            firestore.collection("article").document(picFileName)
                                                    .set(changeData);

                                            //文章各個圖片名稱
                                            washPicData(picDocumentId);
                                            washPicData(picNameArray);
                                            for(int i = 0 ; i < picDocumentId.size() ; i ++){
                                                Map<String,Object> changePicData = new HashMap<>();
                                                changePicData.put("picName" , picNameArray.get(i));
                                                firestore.collection("article").document(picFileName).collection("picName")
                                                        .document(picDocumentId.get(i)).set(changePicData);
                                            }

//                                            //在會員內新增我的文章
//                                            Map<String,Object> addMemberArticle = new HashMap<>();
//                                            addMemberArticle.put("title" , titleET.getText().toString());
//                                            firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("myArticle")
//                                                    .document(picFileName).set(addMemberArticle);

                                            //刪除暫存
                                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).collection("picName")
                                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                        @Override
                                                        public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                                            for(DocumentSnapshot documentSnapshot1 : queryDocumentSnapshots){
                                                                firestore.collection("articleTemp").document(auth.getCurrentUser().getUid()).collection("picName")
                                                                        .document(documentSnapshot1.getId()).delete();
                                                            }
                                                            firestore.collection("articleTemp").document(auth.getCurrentUser().getUid())
                                                                    .delete();
                                                        }
                                                    });

                                        }
                                    });

                            createArticlePicDialog();
                            setProgressTime(progressDialog , 3000);//3秒後

                        }else {
                            confirmAlertDialog.dismiss();
                            createArticleIsSuccess = false;
                            createArticlePicDialog();
                            setProgressTime(progressDialog , 3000);//3秒後
                        }
                    }
                });
    }
}


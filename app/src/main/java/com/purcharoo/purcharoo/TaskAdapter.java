package com.purcharoo.purcharoo;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ExampleViewHolder> {

    private List<ItemTask> list;//清單
    private ExampleViewHolder viewHolder;

    //firebase
    private FirebaseFirestore firestore;

    //點擊效果
    private SearchAdapter.OnItemClickListener clickListener;

    public void setOnItemClickListener(SearchAdapter.OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //載入Task_item.xml
    public static class ExampleViewHolder extends RecyclerView.ViewHolder {

        CircleImageView selfie;
        TextView destination;
        TextView payment;
        TextView distance;

        private ExampleViewHolder(View itemView , final SearchAdapter.OnItemClickListener listener) {
            super(itemView);

            selfie = itemView.findViewById(R.id.selfie);
            destination = itemView.findViewById(R.id.destination);
            payment = itemView.findViewById(R.id.payment);
            distance = itemView.findViewById(R.id.distance);

            //監聽器設置
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    //排序距離(升冪)
    public class ItemTaskComparator implements Comparator<ItemTask>{
        @Override
        public int compare(ItemTask o1, ItemTask o2) {
            return o1.getDistance().compareTo(o2.getDistance());
        }
    }

    public TaskAdapter(List<ItemTask> list) {
        this.list = list;
    }

    //Adapter載入Task_item.xml方法
    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task,
                parent, false);
        viewHolder= new ExampleViewHolder(v, clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ExampleViewHolder holder, final int position) {

        final ItemTask currentItem = list.get(position);

        firestore = FirebaseFirestore.getInstance();

        //載入訂單者照片
       firestore.collection("memberData").document(currentItem.getUserUID()).get()
               .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                   @Override
                   public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                    .fit()
                                    .centerCrop()
                                    .into(holder.selfie);
                        }
                   }
               });
        holder.destination.setText(currentItem.getAddress());
        holder.payment.setText("test");
        holder.distance.setText(decimalFormat("#.##" , currentItem.getDistance()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //取小數點位數
    public String decimalFormat(String pattern , double i){//pattern = 位數 , i = 目標小數

        DecimalFormat df =new DecimalFormat(pattern);
        return df.format(i);
    }

    //計算報酬
    public void calPayment(){

    }

    //新增item
    public void addTask(ItemTask itemTask){
        list.add(itemTask);
        notifyDataSetChanged();
        Collections.sort(list , new ItemTaskComparator());//每次新增完資料便排序
    }

    public List<ItemTask> getList() {
        return list;
    }
}


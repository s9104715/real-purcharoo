package com.purcharoo.purcharoo;

import com.google.firebase.Timestamp;

public class Article {

    private String title;
    private String page;
    private String content;
    private String picFileName;
    private int picNum;
    private String time;
    private String authorAccount;

    public Article() {
    }

    public Article(String title, String page, String content) {
        this.title = title;
        this.page = page;
        this.content = content;
    }

    public Article(String title, String page, String content , String picFileName , int picNum) {
        this.title = title;
        this.page = page;
        this.content = content;
        this.picFileName = picFileName;
        this.picNum = picNum;
    }

    public String getTitle() {
        return title;
    }

    public String getPage() {
        return page;
    }

    public String getContent() {
        return content;
    }

    public String getPicFileName() {
        return picFileName;
    }

    public int getPicNum() {
        return picNum;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    public void setPicNum(int picNum) {
        this.picNum = picNum;
    }

}

package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

public class ChangeGenderActivity extends AppCompatActivity implements View.OnClickListener {

    private String genderTemp;
    private RadioGroup genderRG;
    private RadioButton genderRB;//被選中的RB
    private Button comfirmBnt;

    //firestore
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_change_gender);

        genderRG = (RadioGroup)findViewById(R.id.gender_RG);
        comfirmBnt = (Button)findViewById(R.id.comfirm_bnt);
        comfirmBnt.setOnClickListener(this);

        setupRG();

        //pop up method
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        getWindow().setLayout(1200, 1000);//window size

    }

    public void setupRG(){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        //處理性別資料
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            genderTemp = documentSnapshot.getString("gender");
                            if(genderTemp.equals("男")){
                               genderRG.check(R.id.rb_male);
                            }else if(genderTemp.equals("女")){
                                genderRG.check(R.id.rb_female);
                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {

        //取得性別String
        int getRGId = genderRG.getCheckedRadioButtonId();
        genderRB = (RadioButton)findViewById(getRGId);
        final String gender = (String) genderRB.getText();

        if(gender.equals(genderTemp)){
            super.onBackPressed();//資料一樣不做變動
        }else{
            //修改性別
            Map<String,Object> changedata = new HashMap<>();
            changedata.put("gender" , gender);
            firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                    .set(changedata , SetOptions.mergeFields("gender"));
            finish();
            //更新accSetting介面
            AccSettingActivity.update();
        }
    }
}

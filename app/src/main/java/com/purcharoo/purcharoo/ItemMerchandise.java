package com.purcharoo.purcharoo;


public class ItemMerchandise {

    private String merchandiseName;
    private int unitPrice;

    public ItemMerchandise() {
    }

    public ItemMerchandise(String merchandiseName, int unitPrice) {
        this.merchandiseName = merchandiseName;
        this.unitPrice = unitPrice;
    }

    public String getMerchandiseName() {
        return merchandiseName;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setMerchandiseName(String merchandiseName) {
        this.merchandiseName = merchandiseName;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }
}


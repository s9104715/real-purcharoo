package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.markushi.ui.CircleButton;

public class FragmentTask extends Fragment implements View.OnClickListener {

    private CircleButton orderBnt;
    private CircleButton ackOrderBnt;

    public FragmentTask() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_task, container, false);


        orderBnt = (CircleButton)view.findViewById(R.id.order_bnt);
        ackOrderBnt = (CircleButton)view.findViewById(R.id.ack_order_bnt);
        orderBnt.setOnClickListener(this);
        ackOrderBnt.setOnClickListener(this);

        //code
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.order_bnt:
                startActivity(new Intent(getContext() , OrderActivity.class));
                break;
            case R.id.ack_order_bnt:
                startActivity(new Intent(getContext() , TaskActivity.class));
                break;
        }
    }
}

package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

public class ShareFragmentShareArticle extends Fragment {
    //過濾器元件
    private Spinner schoolSpinner;

    //文章列表元件
    private List<ItemArticle> articles;//文章清單
    private ArrayList<ItemArticle> articlesCompared;//時間排序後的清單
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArticleAdapter articleAdapter;
    private android.support.v7.widget.SearchView searchView;

    //Firebase
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    private static StorageReference storageRef;
    private ArrayList<String> documentIdArray;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_share_article, container, false);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        documentIdArray = new ArrayList<>();
        articles = new ArrayList<>();
        articlesCompared = new ArrayList<>();

        schoolSpinner = view.findViewById(R.id.school_spinner);
        //校園版過濾器設置
        setupSchFilter();

        //文章清單設置
        recyclerView = view.findViewById(R.id.sch_recycler_view);

        //spinner點擊事件
        setupSpinnerOnItemClick();

        //搜尋功能設置
        searchView = view.findViewById(R.id.article_search);
        setupSearchView();

        return  view;
    }

    //校園版過濾器設置
    private void setupSchFilter(){

        firestore.collection("school").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                List<String> schList = new ArrayList<>();
                for(DocumentSnapshot snapshot : queryDocumentSnapshots){
                    schList.add(snapshot.getString("page"));
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext() , R.layout.spinner_item ,schList);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                schoolSpinner.setAdapter(spinnerAdapter);//載入學校清單
            }
        });
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        articleAdapter = new ArticleAdapter(getComparedArticle());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(articleAdapter);

        articleAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //itemclick method
                FragmentShare.aritcleId = getComparedArticle().get(position).getArticleCode();
                startActivity(new Intent(getContext() , ArticleActivity.class));
            }
        });
    }

    private void setupSearchView(){
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setQueryHint("搜尋文章...");
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                articleAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    //讀取文章
    public void loadSchoolArticle(final String page){
        //取得符合校園版的文章文件名稱
        firestore.collection("article").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                articles.clear();
                articlesCompared.clear();
                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    if(documentSnapshot.getString("page").equals(page)){
                        documentIdArray.add(documentSnapshot.getId());
                        articles.add(new ItemArticle(documentSnapshot.getString("userUID") ,
                                documentSnapshot.getString("title") ,
                                documentSnapshot.getString("userAccount") ,
                                timestampToDate(documentSnapshot.get("time" , Timestamp.class)) ,
                                documentSnapshot.getString("picFileName")));
                    }
                }
                setupRecyclerView();
            }
        });
    }

    //spinner點擊事件
    public void setupSpinnerOnItemClick(){
        schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        loadSchoolArticle("輔大版");
                        break;
                    case 1:
                        loadSchoolArticle("政大版");
                        break;
                    case 2:
                        loadSchoolArticle("台大版");
                        break;
                    case 3:
                        loadSchoolArticle("文大版");
                        break;
                    case 4:
                        loadSchoolArticle("淡江版");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public Date timestampToDate(Timestamp timestamp){
        return timestamp.toDate();
    }

    public ArrayList<ItemArticle> getComparedArticle(){

        int afterTime = 0;
        while(articles.size() > 1){
            for(int i = 0 ; i < articles.size() ; i ++){
                for(int j = articles.size() -1 ; j >= 0 ; j--){
                    //找出最晚的時間點
                    if(articles.get(i).getDate().compareTo(articles.get(j).getDate()) > 0){
                        afterTime++;
                    }
                }
                if(afterTime == articles.size() -1){
                    //得到最晚的時間點
                    articlesCompared.add(articles.get(i));
                    articles.remove(i);
                    afterTime = 0;
                }else {
                    afterTime = 0;
                }
            }
        }
        if(articles.size() != 0){
            articlesCompared.add(articles.get(0));
            articles.remove(0);
        }
        return articlesCompared;
    }
}

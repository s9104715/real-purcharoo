package com.purcharoo.purcharoo;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class TabAccessorAdapterGetOrder extends FragmentPagerAdapter {

    public TabAccessorAdapterGetOrder(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int i ){
        switch (i){
            case 0:
                TaskFragmentTasks tab1 = new TaskFragmentTasks();
                return tab1;

            case 1:
                TaskFragmentMyCurrentTasks tab2 = new TaskFragmentMyCurrentTasks();
                return tab2;

            default:
                return null;


        }
    }


    @Override
    public  int getCount(){
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position){
        switch (position){
            case 0:
                return "接受任務";
            case 1:
                return "進行中任務";
            default:
                return null;

        }
    }

}

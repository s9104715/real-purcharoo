package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class ChangeNameActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText nameChangeET;
    private Button confirmBnt;
    private String nameTemp;

    //firestore
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_change_name);

        nameChangeET = (EditText)findViewById(R.id.name_change_et);
        confirmBnt = (Button)findViewById(R.id.comfirm_bnt);
        confirmBnt.setOnClickListener(this);

        setupNmaeET();

        //pop up method
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        getWindow().setLayout(1200, 1000);//window size

    }

    public void setupNmaeET(){

        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        //處理姓名資料
        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                           nameChangeET.setText(documentSnapshot.getString("name"));
                           nameTemp = documentSnapshot.getString("name");//姓名暫存
                        }
                    }
                });
    }

    //確認按鈕點擊方法
    @Override
    public void onClick(View view) {

        if(nameChangeET.getText().toString().trim().isEmpty()){
            nameChangeET.setError("姓名不能為空");
            nameChangeET.requestFocus();
        }else if(nameTemp.equals(nameChangeET.getText().toString().trim())){
            super.onBackPressed();//姓名沒有更動直接回前頁
        }else{
            //修改姓名
            Map<String,Object> changedata = new HashMap<>();
            changedata.put("name" , nameChangeET.getText().toString().trim());
            firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                    .set(changedata , SetOptions.mergeFields("name"));
            finish();
            //更新accSetting介面
            AccSettingActivity.update();
        }

    }
}

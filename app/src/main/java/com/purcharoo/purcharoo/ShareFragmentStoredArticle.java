package com.purcharoo.purcharoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

public class ShareFragmentStoredArticle extends Fragment {

    //文章列表元件
    private List<ItemArticle> articles;//文章清單
    private ArrayList<ItemArticle> articlesCompared;//時間排序後的清單
    private ArrayList<String> articlesId;//儲存收藏文章碼
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ArticleAdapter articleAdapter;
    private android.support.v7.widget.SearchView searchView;
    private TextView noArticleTV;

    //Firebase
    private static FirebaseAuth auth;
    private static FirebaseFirestore firestore;
    private static StorageReference storageRef;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_my_article, container, false);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        articles = new ArrayList<>();
        articlesCompared = new ArrayList<>();
        articlesId = new ArrayList<>();

        //沒文章時的提示
        noArticleTV = view.findViewById(R.id.no_article);

        //文章清單設置
        recyclerView = view.findViewById(R.id.my_recycler_view);
        loadStoredArticle();

        //搜尋功能設置
        searchView = view.findViewById(R.id.my_article_searchview);
        setupSearchView();

        return view;
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        articleAdapter = new ArticleAdapter(getComparedArticle());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(articleAdapter);

        articleAdapter.setOnItemClickListener(new SearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                //itemclick method
                FragmentShare.aritcleId = getComparedArticle().get(position).getArticleCode();
                startActivity(new Intent(getContext() , ArticleActivity.class));
            }
        });
    }

    private void setupSearchView(){
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setQueryHint("搜尋文章...");
        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                articleAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void loadStoredArticle(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid()).collection("storedArticle")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            //收集收藏的文章碼
                            articlesId.add(documentSnapshot.getId());
                        }
                        //以文章碼載入個文章
                        firestore.collection("article").addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                for(DocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                                    for(int i = 0 ; i < articlesId.size() ; i ++){
                                        if(documentSnapshot.getId().equals(articlesId.get(i))){
                                            articles.add(new ItemArticle(documentSnapshot.getString("userUID") ,
                                                    documentSnapshot.getString("title") ,
                                                    documentSnapshot.getString("userAccount") ,
                                                    timestampToDate(documentSnapshot.get("time" , Timestamp.class)) ,
                                                    documentSnapshot.getString("picFileName")));
                                        }
                                    }
                                }
                                setupNoArticleTV();
                                setupRecyclerView();
                            }
                        });
                    }
                });
    }

    public Date timestampToDate(Timestamp timestamp){
        return timestamp.toDate();
    }

    public ArrayList<ItemArticle> getComparedArticle(){

        int afterTime = 0;
        while(articles.size() > 1){
            for(int i = 0 ; i < articles.size() ; i ++){
                for(int j = articles.size() -1 ; j >= 0 ; j--){
                    //找出最晚的時間點
                    if(articles.get(i).getDate().compareTo(articles.get(j).getDate()) > 0){
                        afterTime++;
                    }
                }
                if(afterTime == articles.size() -1){
                    //得到最晚的時間點
                    articlesCompared.add(articles.get(i));
                    articles.remove(i);
                    afterTime = 0;
                }else {
                    afterTime = 0;
                }
            }
        }
        if(articles.size() != 0){
            articlesCompared.add(articles.get(0));
            articles.remove(0);
        }
        return articlesCompared;
    }

    public void setupNoArticleTV(){

        if(articles.size() > 0){
            noArticleTV.setVisibility(View.INVISIBLE);
        }else {
            noArticleTV.setText("您目前還未有收藏文章！");
            noArticleTV.setVisibility(View.VISIBLE);
        }
    }

}

package com.purcharoo.purcharoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Constraints;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import javax.annotation.Nullable;

import at.markushi.ui.CircleButton;
import de.hdodenhof.circleimageview.CircleImageView;

public class ArticleActivity extends AppCompatActivity implements View.OnClickListener {

    //各元件
    private CircleImageView selfie;
    private TextView name;
    private TextView account;
    private TextView date;
    private TextView article;
    private ViewPager picViewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private CircleButton browseBnt;
    private ImageView storedArticleBnt;

    //Firebase
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;
    private StorageReference storageRef;
    public static ArrayList<String> picUri;
    private boolean isStored;//判斷文章是否以儲存
    //processDialog
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_article);

        //各元件設置
        selfie = findViewById(R.id.selfie);
        name = findViewById(R.id.name);
        account = findViewById(R.id.account);
        date = findViewById(R.id.date);
        article = findViewById(R.id.article);
        picViewPager = findViewById(R.id.article_pic);
        browseBnt = findViewById(R.id.browse_bnt);
        storedArticleBnt = findViewById(R.id.stored_article_bnt);

        //firebase初始化
        auth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        storageRef = FirebaseStorage.getInstance().getReference();
        picUri = new ArrayList<>();

        setupTopToolBar();
        //載入文章資料和圖片設置
        setupArticle(FragmentShare.aritcleId);
        setupLove();

        //按鈕點擊方法
        browseBnt.setOnClickListener(this);
        storedArticleBnt.setOnClickListener(this);

    }

    public void setupTopToolBar(){

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.article_toolbar);
        setSupportActionBar(toolbar);
        //toolBar返回鍵
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    //取得各文章資料和設置圖片
    public void setupArticle(final String articleId){

        firestore.collection("article").document(articleId).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){

                            final int picNum = documentSnapshot.get("picNum" , Integer.TYPE);//照片數

                            //setup user
                            firestore.collection("memberData").document(documentSnapshot.getString("userUID")).get()
                                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            Picasso.get().load(documentSnapshot.getString("selfiePath"))
                                                    .fit()
                                                    .centerCrop()
                                                    .into(selfie);
                                            name.setText(documentSnapshot.getString("name"));

                                            if(picNum > 0){
                                                browseBnt.setVisibility(View.VISIBLE);
                                                //讀取圖片uri
                                                firestore.collection("article").document(articleId).collection("picName")
                                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                                                for(DocumentSnapshot documentSnapshot1 : queryDocumentSnapshots){
                                                                    storageRef.child("articlePic/"+articleId+"/" +documentSnapshot1.getString("picName")+ ".png").getDownloadUrl()
                                                                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                                                @Override
                                                                                public void onSuccess(Uri uri) {
                                                                                    picUri.add(uri.toString());
                                                                                    if(picUri.size() == picNum){
                                                                                        washPicData(picUri);
                                                                                        //setup viewpager
                                                                                        viewPagerAdapter = new ViewPagerAdapter(picUri , getApplicationContext() , false);
                                                                                        picViewPager.setAdapter(viewPagerAdapter);
                                                                                    }
                                                                                }
                                                                            });
                                                                }
                                                            }
                                                        });
                                            }else {
                                                //無圖片
                                                //set viewpager height
                                                Constraints.LayoutParams cLp = new Constraints.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT , ConstraintLayout.LayoutParams.WRAP_CONTENT);
                                                cLp.height = 0;
                                                picViewPager.setLayoutParams(cLp);
                                                browseBnt.setLayoutParams(cLp);
                                            }
                                        }
                                    });
                            //文章資料
                            setTitle(documentSnapshot.getString("title"));
                            account.setText(documentSnapshot.getString("userAccount"));
                            date.setText(formatDate(timestampToDate(documentSnapshot.get("time" , Timestamp.class))));
                            article.setText(documentSnapshot.getString("content"));

                        }else {
                            Toast.makeText(getApplicationContext() , "載入文章錯誤！" , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public Date timestampToDate(Timestamp timestamp){
        return timestamp.toDate();
    }

    public String formatDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        return sdf.format(date);
    }

    //把多餘的資料洗掉
    public void washPicData(final ArrayList<String> arrayList) {

        LinkedHashSet<String> set = new LinkedHashSet<>(arrayList);
        ArrayList<String> cleanArrayList = new ArrayList<>(set);//乾淨的arraylist
        //複製
        arrayList.clear();
        arrayList.addAll(cleanArrayList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.browse_bnt:
                startActivity(new Intent(getApplicationContext() , BrowseArticlePicActivity.class));
                break;
            case R.id.stored_article_bnt:
                if(isStored){
                    deleteStoredArticle();
                    //變回無顏色愛心
                }else {
                    //未收藏此文章
                    //storedArticle新增此文章
                    Toast.makeText(getApplicationContext() , "新增" , Toast.LENGTH_LONG).show();
                    Map<String,Object> addStoredArticle = new HashMap<>();
                    addStoredArticle.put("title" , getTitle());
                    firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                            .collection("storedArticle").document(FragmentShare.aritcleId)
                            .set(addStoredArticle);
                    //變紅心
                }
                break;
        }
    }

    public void deleteStoredArticle(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //已收藏此文章
                //文章從storedArticle移除
                firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                        .collection("storedArticle").document(FragmentShare.aritcleId).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("tag" , "ssss");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("tag" , "ffff");
                    }
                });
            }
        }).start();
    }

    //設置愛心顏色
    public void setupLove(){

        firestore.collection("memberData").document(auth.getCurrentUser().getUid())
                .collection("storedArticle").document(FragmentShare.aritcleId).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if(documentSnapshot.exists()){
                            //紅愛心
                            isStored = true;
                        }else {
                            isStored = false;
                        }
                    }
                });
    }
}
